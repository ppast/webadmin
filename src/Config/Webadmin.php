<?php

// namespace
namespace Ppast\Webadmin\Config;



// classe de base pour config webadmin
class Webadmin extends Base
{
	// méthode statique de construction : nom du profil et tableau associatif
	public static function setup($webadmin, $params)
	{
		WebAdminConfig::$WEBADMINS[$webadmin] = new Webadmin($webadmin, $params);
	}
	
	
	// lire la config d'un webadmin (utilisé quand on en invoque un, on a besoin de sa config tout de suite)
	public static function setupFromFile($webadmin)
	{
		if ( array_key_exists($webadmin, WebAdminConfig::$WEBADMINS) && ($web = WebAdminConfig::$WEBADMINS[$webadmin]) )
			return $web;
		else
		{
			$f = WebAdminConfig::$DATA_ROOT . "config_webadmins/" . $webadmin . '.php';
			if ( !file_exists($f) )
				return NULL;
				
			include_once($f);
			
			// prendre le dernier élément ajouté, c'est celui-ci que l'on veut
			$web = end(WebAdminConfig::$WEBADMINS); 
			return $web;
		}
	}	
}

?>