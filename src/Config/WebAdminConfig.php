<?php

// namespace
namespace Ppast\Webadmin\Config;



// classe centrale pour config console
class WebAdminConfig extends Core
{
	// liste des applications
	static $APPS = array(); 
	
	// liste des domaines
	static $DOMAINS = array();
	
	// liste des webadmins
	static $WEBADMINS = array();
	
	// liste des plugins pour l'application sélectionnée
	static $PLUGINS = array();
		


	// intégrer la liste des plugins demandée, à partir du dossier de configuration des plugins
	static function listPlugins($plugins)
	{
		foreach ( $plugins as $plugin )
			if ( file_exists(self::$DATA_ROOT . "config_plugins/{$plugin}.php") )
				include_once(self::$DATA_ROOT . "config_plugins/{$plugin}.php");
	}
	

	// obtenir la liste des plugins par défaut (liste chaine, pas config)
	static function defaultPlugins()
	{
		$ret = glob(rtrim(dirname(__FILE__),'/') . '/../WebadminCommands/Plugins/*');
		if ( is_array($ret) )
			return array_map(function($a){return basename($a, '.php');}, $ret);
		else
			return array();
	}
	

	// faire la liste des applications, à partir du dossier de configuration
	static function listApps()
	{
		$apps = glob(self::$DATA_ROOT . 'config_profiles/*.php');
		if ( is_array($apps) )
		{
		    $regs = [];
			foreach ( $apps as $app )
				if ( preg_match('~config_profiles/(.*)\.php$~', $app, $regs) )
					include(self::$DATA_ROOT . 'config_profiles/' . $regs[1] . '.php');
		}
	}
	

	// faire la liste des domaines, à partir du dossier de configuration
	static function listDomains()
	{
		$apps = glob(self::$DATA_ROOT . 'config_domains/*.php');
		$regs = [];
		if ( is_array($apps) )
			foreach ( $apps as $app )
				if ( preg_match('~config_domains/(.*)\.php$~', $app, $regs) )
					include(self::$DATA_ROOT . 'config_domains/' . $regs[1] . '.php');
	}
}



?>