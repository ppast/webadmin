<?php

// namespace
namespace Ppast\Webadmin\Config;



// classe de base pour plugins
class Plugin extends Base
{
	// méthode statique de construction : nom du profil et tableau associatif
	public static function setup($plugin, $params)
	{
		WebAdminConfig::$PLUGINS[$plugin] = new Plugin($plugin, $params);
	}
	
	
	// lire la config d'un plugin (utilisé quand on en invoque un, on a besoin de sa config tout de suite)
	public static function setupFromFile($plugin)
	{
		if ( array_key_exists($plugin, WebAdminConfig::$PLUGINS)  && ($plug = WebAdminConfig::$PLUGINS[$plugin]) )
			return $plug;
		else
		{
			include_once(WebAdminConfig::$DATA_ROOT . "config_plugins/" . $plugin . '.php');
			
			// prendre le dernier élément ajouté, c'est celui-ci que l'on veut
			$plugin = end(WebAdminConfig::$PLUGINS); 
			return $plugin;
		}
	}	
	
	
	// lire les plugins énumérés
	public static function setupFromList($list)
	{
		WebAdminConfig::listPlugins($list);
	}
}


?>