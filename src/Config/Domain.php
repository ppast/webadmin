<?php

// namespace
namespace Ppast\Webadmin\Config;



// classe de base pour config domaine
class Domain extends Base
{
	// méthode statique de construction : nom du profil et tableau associatif
	public static function setup($domain, $params)
	{
		WebAdminConfig::$DOMAINS[$domain] = new Domain($domain, $params);
	}
}


?>