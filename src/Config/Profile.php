<?php

// namespace
namespace Ppast\Webadmin\Config;



// classe de base pour config profil
class Profile extends Base
{
	// méthode statique de construction : nom du profil et tableau associatif
	public static function setup($profile, $params)
	{
		WebAdminConfig::$APPS[$profile] = new Profile($profile, $params);
	}
}



?>