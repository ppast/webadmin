<?php

// namespace
namespace Ppast\Webadmin\Includes;


// clauses use
use \Ppast\Webadmin\Config\WebAdminConfig;



// classe
class Tools
{
	
	// tester sécurité profil
	static function testSec($appcfg)
	{
		return self::testSecFolder(Tools::ensureTrailingSlash(WebAdminConfig::$ROOT) . Tools::ensureNoTrailingSlash($appcfg->SEC__ROOT?$appcfg->SEC__ROOT:'') . $appcfg->SEC__TEST_FOLDER);
	}
	
	
	// tester sécurité profil en testant un dossier
	static function testSecFolder($folder)
	{
		if ( file_exists($folder) )
			return substr(sprintf("%o", fileperms($folder)), -4) == '0505';
		else
			true;
	}
	
	
	// tester activation optimisation site
	static function testOpt($appcfg)
	{
		// lecture du manifest
		$m = Tools::readManifest($appcfg);
		if ( !$m )
			return "Manifest illisible '" . $appcfg->APP_MANIFEST . "'.";
			
		$regs = [];
		if ( preg_match('/Manifest::MINIFY[ ]*=>[ ]*(true|false)/', $m['manifest'], $regs) )
			return $regs[1] == 'true';
		else
			return NULL;
	}


	// s'assurer que le dossier se termine par un slash
	static function ensureTrailingSlash($folder)
	{
		return IncludesHelper::ensureTrailingSlash($folder);
	}


	// s'assurer que le dossier ne se termine pas par un slash
	static function ensureNoTrailingSlash($folder)
	{
		return IncludesHelper::ensureNoTrailingSlash($folder);
	}


	// lecture manifest
	static function readManifest($appcfg)
	{
		$f = Tools::ensureTrailingSlash(WebAdminConfig::$ROOT) . Tools::ensureTrailingSlash($appcfg->APP_ROOT?$appcfg->APP_ROOT:'') . $appcfg->APP_MANIFEST;
		if ( !file_exists($f) )
			return NULL;
		else
			return array('manifest' => file_get_contents($f), 'filename' => $f);
	}


	// obtenir paramètres config globaux du manifest
	static function readManifestConfig($appcfg)
	{
	    $regs = [];
		if ( $m = self::readManifest($appcfg) )
			if ( preg_match('~Manifest::CONFIG[ ]*=>[ ]*array[ ]*\(([^)]*)\),~', $m['manifest'], $regs) )
			{
				// Une fois la chaine obtenue (ex. 'no_google_api' => false,\n'google_api_timeout' => 10) découper
				if ( trim($regs[1]) != '' )
				{
					$cfg = array();
					$params = explode(',', $regs[1]);
					foreach ( $params as $p )
					{
						// découper clef / valeur
						$pr = explode('=>', $p);
						
						// enlever " ou ' terminaux
						$cfg[] = mb_substr(trim($pr[0]), 1, mb_strlen(trim($pr[0])) - 2) . '=' . trim($pr[1]);
					}
					
					return implode("\n", $cfg);
				}
				else
					return "";
			}
			else
				return "";
		else
			return NULL;
	}


	// définir paramètres config globaux du manifest
	static function writeManifestConfig($cfg, $appcfg)
	{
		// lecture du manifest
		$m = Tools::readManifest($appcfg);
		if ( !$m )
			return "Manifest illisible '" . $appcfg->APP_MANIFEST . "'.";
			
	
		$ret = array();
		
		// si des choses à enregistrer
		if ( $cfg = trim($cfg) )
		{
			// découper la chaine en lignes
			$lines = explode("\n", $cfg);
			foreach ( $lines as $l )
				if ( $l = trim($l) )
				{
					// découper key=valeur et exclure si pas pu découper en exactement 2 parties key=valeur
					$config = explode('=', $l);
					if ( count($config) == 2 )	
					{
						// prendre valeur du paramètre
						$value = trim($config[1]);
						
						// traiter la syntaxe : laisser true/false/NULL et les entiers ou flottants tels quels, et mettre tout le reste entre quote / guillemets
						if ( !preg_match('#^(true|false|NULL|[0-9]+((,|\\.)[0-9]+)?|\'.*\'|".*")$#', $value) )
							$value = "'" . $value . "'";
							
						$ret[] = "'" . trim($config[0]) . "' => " . $value;
					}
				}
		}

		// modifier manifest et l'écrire		
		$txt = preg_replace('~Manifest::CONFIG[ ]*=>[ ]*array[ ]*\([^)]*\),~', "Manifest::CONFIG => array(\n" . implode(",\n", $ret) . "\n),", $m['manifest']);

		$fi = fopen($m['filename'], 'w');
		fwrite($fi, $txt);
		fclose($fi);

		return NULL;
	}
	
	
	// minifier avec Google Closure Compiler
	static function minifyWithGoogleClosureCompiler($source, $output, $language_in = 'ECMASCRIPT5')
	{
		// créer la connexion et la réutiliser ensuite
		$c = curl_init(); 

		// constituer URL
		$url = "js_code=" . urlencode(file_get_contents($source)) . '&';
		
		curl_setopt($c, CURLOPT_URL, 'https://closure-compiler.appspot.com/compile');
		
		// on fait une requête POST
		curl_setopt($c, CURLOPT_POST, true);
		
		// on veut récupérer les données renvoyées par le serveur
		curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
		
        // suivi
        curl_setopt($c, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($c, CURLOPT_POSTREDIR, CURL_REDIR_POST_ALL);

		// les champs
		curl_setopt($c, CURLOPT_POSTFIELDS, $url . 
				implode('&',
					array_filter(
							array(
							'charset=UTF-8',
							'compilation_level=SIMPLE_OPTIMIZATIONS',
							'output_format=json',
							'output_info=compiled_code',
							'output_info=errors',
							$language_in ? "language=$language_in" : NULL
						)
					)
				)
			);
		
		// timeout
		curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($c, CURLOPT_TIMEOUT, 20);
		
		// exécuter la requête
		$curloutput = curl_exec ($c);
		
		// obtenir un code d'erreur HTTP
		$i = curl_getinfo($c,  CURLINFO_HTTP_CODE);
		
		// HTTP autre que 200 : erreur
		if ( $i != 200 ) 
		{
			$ret = "Erreur HTTP $i : " . curl_error($c);
			curl_close($c);
			return $ret;
		}


		// décoder réponse json
		$json = json_decode($curloutput, true);
		
		
		// si erreurs (erreur parser)
		if ( array_key_exists('errors', $json) && is_array($json['errors']) && count($json['errors']) )
		{
			$msg = 'Erreur(s) optimisation : ';
			foreach ( $json['errors'] as $err )
				$msg .= 'Erreur ' . $err['error'] . '  -  fichier ' . $err['file'] . '  -  ligne ' . $err['lineno'] . '  :  ' . $err['line'] . "\n";
			
			curl_close($c);	
			return $msg;
		}
		
		
		// si erreur du service
		if ( array_key_exists('serverErrors', $json) && is_array($json['serverErrors']) && count($json['serverErrors']) )
		{
			// prendre la première erreur
			$err = $json['serverErrors'][0];
			return 'Fichier non optimisé (Erreur Google Closure API : ' . $err['code'] . '  -  ' . $err['error'] . ")\n";
		}
		

		// sinon, tout va bien
		$fi = fopen($output, 'w');

		// si version ecmascript stricte
		if ( strpos($language_in, '_STRICT') !== FALSE )
			fwrite($fi, "'use strict';\n");

		fwrite($fi, $json['compiledCode']);
		fclose($fi);
				
				
		// pas d'erreur
		curl_close($c);
		return false;
	}
	
	
	// minifier avec librairie PHP
	static function minifyWithPHP($source, $output)
	{
		// chemin vers matthiasmullie/minify : tableau d'includes
		if ( $libminify = WebAdminConfig::$ROOT_USER_CFG->MINIFY_INCLUDES )
		{
			// inclure les fichiers
			foreach ( $libminify as $lib )
				include_once $lib;
				
			// tester inclusion ok
			if ( !class_exists('\MatthiasMullie\Minify\JS') )
				return 'Librairie \'matthiasmullie/minify\' non incluse.';


			try
			{
				
				// minifier le fichier
				$minifier = new \MatthiasMullie\Minify\JS(file_get_contents($source));
				$minifier->minify($output);
				
				// pas d'erreur
				return FALSE;
			}
			catch (\Exception $e)
			{
				return $e->getMessage();
			}
		}
		else
			return 'Librairie \'matthiasmullie/minify\' indisponible.';
	}
	
	
	// minifier avec google closure compiler et, par défaut, librairie PHP
	static function minify($source, $output, $kind = 'js', $language_in = 'ECMASCRIPT5')
	{
		// si optimisation JS
		if ( $kind == 'js' )
		{	
			
			if ( $st1 = self::minifyWithGoogleClosureCompiler($source, $output, $language_in) )
			{
				if ( $st2 = self::minifyWithPHP($source, $output) )
					return $st2;
				else
					// ok, mais fournir le feedback de google
					return array('Optimisé avec MatthiasMullie\\Minify', 'Erreur avec Google Closure Compiler : ' . $st1);
			}
			else
				return array('Optimisé avec Google Closure Compiler');
		}
		
		
		// si optimisation CSS
		else if ( $kind == 'css' )
		{
			if ( $file = file_get_contents($source) )
			{
				// remplacer les espaces consécutifs par 1 seul, les sauts de ligne par un espace, les commentaires par un espace
				$file = preg_replace(array("/[ \t]+/", '/\n/', '#/\*.+\*/#U'), ' ', $file);
				$file = preg_replace('/\s\s+/', ' ', $file);
				$fi = fopen($output, "w");
				fwrite($fi, $file);
				fclose($fi);

				// fichier optimisé				
				return array(str_replace(Tools::ensureTrailingSlash(WebAdminConfig::$ROOT), '', $source));
			}
			else
				return "Erreur à l'optimisation CSS de '$source' : fichier vide.";
		}
		
		
		// optimisation inconnue
		else
			return "Type d'optimisation inconnu : '$kind'";
	}
	
	
	// formater une chaine et l'afficher dans une console noir et blanc
	static function consoleOutput($str)
	{
		$str = str_replace(array("\r\n","\n"), '<br>', $str?$str:'');
		$html = <<<"HTML"
<div class="console">
$str
</div>
HTML;
		return $html;
	}
}


?>