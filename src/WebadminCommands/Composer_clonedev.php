<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Domain;
use \Ppast\Webadmin\Config\WebAdminConfig;
use \Ppast\Webadmin\Includes\Tools;
// clauses use]



class Composer_clonedev extends \Ppast\Webadmin\Commands\Base
{
	public function run(Domain $domcfg)
	{
		// dossier cible et sources
		$source = Tools::ensureTrailingSlash(Tools::ensureTrailingSlash(WebAdminConfig::$ROOT) . $_REQUEST['libc']) . 'vendor/' . $_REQUEST['package'] . '/';
		$devc = Tools::ensureTrailingSlash(Tools::ensureTrailingSlash(WebAdminConfig::$ROOT) . $domcfg->COMPOSER_DEV_PATH) . $_REQUEST['package'] . '/';


		// vérifier que le dossier cible n'existe pas
		if ( file_exists($devc) )
			return $this->status(false, 'Le dossier de développement cible existe déjà ; clônage abandonné');


		// créer le dossier cible ; copier les fichiers
		$script = "mkdir -p {$devc} ; cp -r {$source}* $devc";
		$ret = shell_exec("$script 2>&1");

		if ( $ret && trim($ret) )
			return $this->status(false, 'Erreur au clônage du package.', Tools::consoleOutput($ret), true);
		

        try
        {
            // supprimer le package dans sa version packagist
            $ret = \Ppast\Webadmin\WebadminCommands\Helpers\ComposerInterface::get()->package_remove($_REQUEST['package']);
            if ( mb_strpos($ret, WebAdminConfig::$ROOT_CFG->COMPOSER_REMOVE_OK) === FALSE )
                return $this->status(false, 'Erreur à la suppression de la version publique du package clôné', Tools::consoleOutput($ret), true);


            // ajoute maintenant un repository
            $ret = \Ppast\Webadmin\WebadminCommands\Helpers\ComposerInterface::get()->repository_add('path', '../' . Tools::ensureTrailingSlash($domcfg->COMPOSER_DEV_PATH) . $_REQUEST['package'] . '/');

            // ajouter le package clôné en dev
            $ret = \Ppast\Webadmin\WebadminCommands\Helpers\ComposerInterface::get()->package_require($_REQUEST['package'] . ':@dev');
            return $this->status(true, 'Clone_dev exécuté', Tools::consoleOutput($ret), true);
        }
        catch(\Throwable $e)
        {
            return $this->status(false, 'Une exception est survenue : ', Tools::consoleOutput($e->getMessage()), true);
        }

	}
	
}


?>