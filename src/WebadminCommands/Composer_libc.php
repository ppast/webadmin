<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Domain;
// clauses use]



class Composer_libc extends \Ppast\Webadmin\Commands\Base
{
	public function run(Domain $domcfg)
	{
		return $this->status(true, 'Librairie Composer \'' . rtrim($_REQUEST['libc'], '/') . '\' sélectionnée');
	}
	
}


?>