<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Domain;
use \Ppast\Webadmin\Config\WebAdminConfig;
// clauses use]


use \Ppast\Webadmin\WebAdminWebInterface\Controller;



class Dump_web extends \Ppast\Webadmin\Commands\Base
{
	public function run(Domain $domcfg)
	{
		$files = WebAdminConfig::$ROOT . '*';
		$f = WebAdminConfig::$ROOT . '_dump_' . $domcfg->name . '.zip';
		$out = shell_exec("zip -q -r $f $files");
			
		return $this->status(true, 'Sauvegarde site demandée');
	}
	
}


?>