<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Profile;
use \Ppast\Webadmin\Config\WebAdminConfig;
// clauses use]



class Opt_js extends \Ppast\Webadmin\Commands\Base
{
	public function run(Profile $appcfg)
	{
		// on fait d'abord la liste des fichiers
		$data = array('files'=>array());
		if ( $st = Helpers\Opt::_folder(WebAdminConfig::$ROOT . $appcfg->OPT__ROOT, '*.js', array('\Ppast\Webadmin\WebadminCommands\Helpers\Opt', '_minify_js'), $data, $appcfg) )
			return $this->status(false, $st);

		// puis on constitue des batchs
		$data2 = array('batches' => NULL);
		if ( $st = Helpers\Opt::_minify_js_pass2($data['files'], $data2, $appcfg) )
			return $this->status(false, $st);
		
		// puis on envoie la requete
		$data3 = array('count' => 0, 'feedback' => '', 'mode_degrade'=> false);
		if ( $st = Helpers\Opt::_minify_js_pass3($data2['batches'], $data3, $appcfg) )
			return $this->status(false, $st);
		
		// consigner nouvelle version
		return $this->status(true, "Traitement effectué : " . $data3['count'] . ' fichier(s) optimisé(s)' . ($data3['mode_degrade'] ? ' <u>en mode dégradé</u>.':'.'), $data3['feedback']);
	}
	
}


?>