<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Domain;
use \Ppast\Webadmin\Config\WebAdminConfig;
use \Ppast\Webadmin\Includes\Tools;
// clauses use]



class Composer_devzip extends \Ppast\Webadmin\Commands\Base
{
	public function run(Domain $domcfg)
	{
		// chemin de dev
		$devc = Tools::ensureTrailingSlash(Tools::ensureTrailingSlash(WebAdminConfig::$ROOT) . $domcfg->COMPOSER_DEV_PATH);

		// package vendor/name (transformé en vendor-name pour conformité systeme de fichiers)
		$package = $_REQUEST['package'];
		$friendlypackagename = str_replace('/', '!', $package);
		
		// chemin fichier temporaire
		$ztmp = sys_get_temp_dir() . "/{$friendlypackagename}.zip";

		// script et exécution		
		$script = "cd {$devc} ; zip -r $ztmp {$package}/*";
		$ret = shell_exec($script);
		
		if ( file_exists($ztmp) )
		{
			header("Content-Type: application/zip; name=\"{$friendlypackagename}.zip\"");
			header("Content-Disposition: attachment; filename=\"{$friendlypackagename}.zip\"");
			header("Expires: 0");
			header("Cache-Control: no-cache, must-revalidate");
			header("Pragma: no-cache"); 
	
			readfile($ztmp);
			unlink($ztmp);
			die();
		}
		else
			return $this->status(false, 'Impossible de créer une archive pour ce package en développement', Tools::consoleOutput($ret), true);
	}
	
}


?>