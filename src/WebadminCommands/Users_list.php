<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;



// [clauses use

use \Ppast\Webadmin\Config\Core;

// clauses use]



class Users_list extends \Ppast\Webadmin\Commands\Base
{
	// affichage
	protected function _renderCSS_JS()
	{
		parent::_renderCSS_JS();
		
		?><script>
	
			window.webadmin = window['webadmin'] || {};
			webadmin.users = webadmin['users'] || {
                
                // obtenir littéral objet depuis ligne saisie
                getUser : function(tr)
                {
                    var inputs = tr.querySelectorAll("td input[type='text']");
                    var uobject = {};
                    var inputsl = inputs.length;

                    // mémoriser valeur champ dans propriété user
                    for ( var i = 0 ; i < inputsl ; i++ )
                        uobject[inputs[i].getAttribute('data-property')] = inputs[i].value;


                    return uobject;
                },
                
                
                // modifier le mot de passe
                newPassword : function(event)
                {
					var target = nettools.jscore.getTarget(event);
                    
                    var passwd = nettools.ui.desktop.dialog.promptPromise('Nouveau mot de passe ?', '').
						then(
							function(passwd)
							{
								if ( passwd )
								{
									target.parentNode.querySelector('input').value = nettools.jscore.sha256(passwd);

									nettools.ui.desktop.dialog.notify('Mot de passe mémorisé ; valider les changements avec les outils "." ou "..." en bout de ligne');
								}
							}
						);
                },
                
                
                // actions
				click : function(event)
				{
					var target = nettools.jscore.getTarget(event);
					
					// déterminer origine clic
					if ( target.nodeName == 'INPUT' )
					{
						// remonter à la valeur
						var user = target.parentNode.parentNode.getAttribute('data-user');
	
	
						if ( target.type == 'button' )
						{
							switch ( target.value )
							{
								// si ajout
								case '+' :
                                    
                                    // obtenir littéral objet depuis ligne de saisie
                                    var uobject = webadmin.users.getUser(target.parentNode.parentNode);
                                    
                                    
                                    // traiter encodage mot de passe
                                    uobject.password = nettools.jscore.sha256(uobject.password);
                                    
                                    
									// vérifier saisie : au moins user et password
									if ( uobject.name && uobject.password )
										nettools.jscore.SecureRequestHelper.post(
												// URL
												'<?php echo \Ppast\Webadmin\WebInterface\CoreController::$routage; ?>',
												
												// data
												nettools.jscore.mergeObjects(
                                                        {
                                                            u : '<?php echo \Ppast\Webadmin\WebInterface\CoreController::$USER; ?>',
                                                            webadmin : 'utilisateurs',
                                                            cmd : 'users_new'
                                                        }, 
                                                    
                                                        // littéral objet user
                                                        uobject
                                                    )
											);

									break;
								
									
								// si modif mot de passe ou niveau d'accès pour tous les sites gérés par la console
								case '...' :

                                    // obtenir littéral objet depuis ligne de saisie
                                    var uobject = webadmin.users.getUser(target.parentNode.parentNode);
                                    

                                    // vérifier saisie : au moins user et password
									if ( uobject.name && uobject.password )
                                        nettools.ui.desktop.dialog.confirmPromise('Confirmer les modifications de l\'utilisateur \'' + user + '\' pour tous les sites gérés par la console d\'administration.').
											then(
												function(r)
												{
													var req = new URLSearchParams(uobject);
													var qs = 'cmd=users_update_console&_user=' + user + '&' + req.toString();
													parent.postMessage(qs, '*');
												}
											);
									
                                    break;

								
								// si modif mot de passe ou niveau d'accès
								case '.' :

                                    // obtenir littéral objet depuis ligne de saisie
                                    var uobject = webadmin.users.getUser(target.parentNode.parentNode);
                                    

                                    // vérifier saisie : au moins user et password
									if ( uobject.name && uobject.password )
										nettools.jscore.SecureRequestHelper.post(
												// URL
												'<?php echo \Ppast\Webadmin\WebInterface\CoreController::$routage; ?>',
												
												// data
												nettools.jscore.mergeObjects(
                                                        {
                                                            u : '<?php echo \Ppast\Webadmin\WebInterface\CoreController::$USER; ?>',
                                                            webadmin : 'utilisateurs',
                                                            cmd : 'users_update',
                                                            _user : user  // nom de l'utilsiateur avant chgt éventuel
                                                        }, 
                                                    
                                                        // littéral objet user
                                                        uobject
                                                    )
											);
                                    
									break;
									
									
								// si suppression
								case 'x' :
									nettools.ui.desktop.dialog.confirmPromise('Supprimer l\'utilisateur \'' + user + '\' ?').
										then(
											function()
											{
												nettools.jscore.SecureRequestHelper.post(
													// URL
													'<?php echo \Ppast\Webadmin\WebInterface\CoreController::$routage; ?>',

													// data
													{
														u : '<?php echo \Ppast\Webadmin\WebInterface\CoreController::$USER; ?>',
														webadmin : 'utilisateurs',
														'user' : user,
														cmd : 'users_delete'
													}
												);
											}
										);

									break;
							}
						}
					}
				}
			};
			
			</script><style>

			.webadmin_users input[type='text'] {
                width:275px;
			}

			</style><?php
	}
	

	// affichage
	protected function _renderBody()
	{
		echo '<table cellpadding="0" cellspacing="0" border="0" class="webadminList webadmin_users" onclick="return webadmin.users.click(event);">' . "\n";


        // en-tetes : énumérer les propriétés
		echo '<tr>';
        
        foreach ( $this->_body['properties'] as $prop )
            echo "<th>$prop</th>";
        
        echo "<th>&nbsp;</th></tr>\n";

        
		// ligne nouveau user
		echo '<tr class="newline">';

        foreach ( $this->_body['properties'] as $prop )
            echo '<td><input type="text" data-property="' . $prop . '" placeholder="Nouvel utilisateur"></td>';
        
        echo '<td><input type="button" value="+"></td></tr>' . "\n";
		

		// lister users
		foreach ( $this->_body['users'] as $user )
        {
            // mémoriser clef (username) au cas changement ensuite
			echo '<tr data-user="' . $user->name . '">';
            
            // énumérer les propriétés
            foreach ( $this->_body['properties'] as $prop )
                // si mot de passe traiter spécifiquement
                if ( $prop == 'password' )
                    echo '<td><a href="javascript:(0)" onclick="webadmin.users.newPassword(event); return false;">modifier le mot de passe</a><input style="visibility:hidden; display:none;" type="text" value="' . $user->{$prop} . '" data-property="' . $prop . '"></td>';
                else
                    echo '<td><input type="text" value="' . $user->{$prop} . '" data-property="' . $prop . '"></td>';

            echo '<td><input type="button" value="."><input type="button" value="..."><input type="button" value="x"></td></tr>';
        }

		echo '</table>';
	}

	
	public function run()
	{
		$data = array('count'=>0, 'body'=>'');
	
		// obtenir liste
		$users = \Ppast\Webadmin\Config\Users::usersProvider()->listUsers();

		if ( count($users) )
		{
			$data['body'] = array('users'=>$users, 'properties'=>\Ppast\Webadmin\Config\Users::usersProvider()->userProperties());
			$data['count'] = count($users);
		}
		
		
		return $this->status(true, "Liste des utilisateurs ci-dessous : " . $data['count'] . ' utilisateur(s).', $data['body'], true);
	}
	
}


?>