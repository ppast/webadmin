<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Profile;
// clauses use]



class Sec_on extends \Ppast\Webadmin\Commands\Base
{
	public function run(Profile $appcfg)
	{
		if ( $st = Helpers\Opt::_secOnOff(true, $appcfg) )
			return $this->status(false, $st);
		
		return $this->status(true, "Verrouillage du profil activé.");
	}
	
}


?>