<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\WebAdminConfig;
use \Ppast\Webadmin\Includes\Tools;
// clauses use]




class Jsonp_site_status extends \Ppast\Webadmin\Commands\Base
{
	public function run()
	{
		// répondre en fournissant, entre autres, le n°index donné en paramètre
		$data = array("index"=>(integer)($_REQUEST['i']));
		$data['secured'] = true;


        // interroger sécurité de chaque appli du site
		foreach ( WebAdminConfig::$APPS as $k=>$a )
		{
			// tester sécurité de ce profil
			if ( !Tools::testSec($a) )
			{
				$data['secured'] = false;
				break;
			}
		}
        
        
        // assertions
        if ( WebAdminConfig::$ROOT_USER_CFG->CONSOLE_CHECKS )
        {
            $data['ko_checks'] = array();
            
            foreach ( WebAdminConfig::$ROOT_USER_CFG->CONSOLE_CHECKS as $k => $chk )
                if ( is_callable($chk) )
                {
                    try
                    {
                        if ( !$chk() )
                            $data['ko_checks'][] = $k;
                    }
                    catch(\Throwable $e)
                    {
                        $data['ko_checks'][] = "$k (callback exception)";
                    }
                }
                else
                    if ( !$chk )
                        $data['ko_checks'][] = $k;
        }
        
        
        
        // fournir version PHP et type hébergement
        $data['env'] = WebAdminConfig::$ROOT_USER_CFG->ENV; 

		return $this->status(true, NULL, $data);
	}
	
}


?>