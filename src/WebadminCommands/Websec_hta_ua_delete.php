<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Domain;
// clauses use]



class Websec_hta_ua_delete extends \Ppast\Webadmin\Commands\Base
{
	public function run(Domain $domcfg)
	{
		if ( $st = Helpers\Websec::_websec_ua_update($domcfg, $_REQUEST['ua'], true) )
			return $this->status(false, $st);
		else	
			return $this->status(true, "Suppression du user-agent proscrit '" . $_REQUEST['ua'] . "' effectuée.");
	}
	
}


?>