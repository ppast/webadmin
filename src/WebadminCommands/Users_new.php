<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use

// clauses use]



class Users_new extends \Ppast\Webadmin\Commands\Base
{
	public function run()
	{
        // énumérer propriétés du user
        $user = array();

        foreach ( \Ppast\Webadmin\Config\Users::usersProvider()->userProperties() as $prop )
            $user[$prop] = $_REQUEST[$prop];
        
        // créer user
		\Ppast\Webadmin\Config\Users::usersProvider()->createUser((object) $user);
		
	
		return $this->status(true, "Utilisateur correctement créé.");
	}
	
}


?>