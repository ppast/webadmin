<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Domain;
use \Ppast\Webadmin\Config\WebAdminConfig;
// clauses use]


use \Ppast\Webadmin\WebAdminWebInterface\Controller;



class Websec_ua_j extends \Ppast\Webadmin\Commands\Base
{
	// affichage
	protected function _renderCSS_JS()
	{
		parent::_renderCSS_JS();


		// récuperer paramètres
		$networktools = WebAdminConfig::$ROOT_CFG->LOG__NETWORK_TOOLS;
		$gsearch_ua = WebAdminConfig::$ROOT_CFG->LOG__UA;

		// config depuis contexte controller
		$domcfg = WebAdminConfig::$DOMAINS[Controller::$CONTEXT->value];

		
?><script>
			window.webadmin = window['webadmin'] || {};
			webadmin.websec = webadmin['websec'] || {
				click : function(event)
				{
					var target=nettools.jscore.getTarget(event);

					// déterminer origine clic
					if ( target.nodeName == 'INPUT' )
					{
						// remonter au champ de saisie
						var input = target;
						while ( input && (input.type != 'text') )
							input = input.previousSibling;


						// si IP
						if ( input && (target.parentNode.className.indexOf('ip') != -1) )
						{
							if ( target.type == 'button' )
							{
								switch ( target.value )
								{
									case '?' :
										window.open(('<?php echo $networktools; ?>').replace(/%/, input.value), 'network_tools');
										break;
										
										
									// si ajout uniquement dans ce site
									case '+' :
										// autoriser l'ajout si pas encore proscrit
										if ( input.parentNode.className.indexOf('highlight2') >= 0 )
											nettools.jscore.SecureRequestHelper.post(
													// URL
													'<?php echo \Ppast\Webadmin\WebInterface\CoreController::$routage; ?>',
													
													// data
													{
														u : '<?php echo \Ppast\Webadmin\WebInterface\CoreController::$USER; ?>',
														dom : '<?php echo $domcfg->name; ?>',
														ip : input.value,
														cmd : 'websec_ip_update'
													}
												);
										break;
										
										
									// si ajout dans tous les sites gérés par la console
									case '++' :
										nettools.ui.desktop.dialog.confirmPromise('Interdire cette IP sur tous les sites gérés par la console d\'administration ?'). 
											then(
												function()
												{
													parent.postMessage('cmd=websec_ip_update_console&ip=' + input.value, '*');
												}
											);
											
										break;
								}
							}
						}
						
						// si User Agent
						else if ( target.parentNode.className.indexOf('ua') != -1 )
						{
							if ( target.type == 'button' )
							{
								switch ( target.value )
								{
									case '?' :
										// si pas de sélection, champ entier, sinon on extrait
										if ( input.selectionStart != input.selectionEnd )
											var ua = nettools.jscore.trim(input.value.substring(input.selectionStart, input.selectionEnd)) + '+user+agent';
										else
											var ua = input.value;
											
										window.open(('<?php echo $gsearch_ua; ?>').replace(/%/, ua), 'ua');
										break;
										

									// si ajout uniquement dans ce site
									case '+' :
										// autoriser l'ajout si pas encore proscrit
										if ( input.parentNode.className.indexOf('highlight2') >= 0 )
											// si délimitation par sélection à la souris du useragent
											if ( input.selectionStart != input.selectionEnd )
											{
												var ua = nettools.jscore.trim(input.value.substring(input.selectionStart, input.selectionEnd));

												nettools.jscore.SecureRequestHelper.post(
														// URL
														'<?php echo \Ppast\Webadmin\WebInterface\CoreController::$routage; ?>',
														
														// data
														{
															u : '<?php echo \Ppast\Webadmin\WebInterface\CoreController::$USER; ?>',
															dom : '<?php echo $domcfg->name; ?>',
															'ua' : ua,
															cmd : 'websec_ua_update'
														}
													);
											}
										break;
										
										
									// si ajout dans tous les sites gérés par la console
									case '++' :
										// si délimitation par sélection à la souris du useragent
										if ( input.selectionStart != input.selectionEnd )
											// autoriser l'ajout si pas encore proscrit
											nettools.ui.desktop.dialog.confirmPromise('Interdire ce user-agent sur tous les sites gérés par la console d\'administration ?'). 
												then(
													function()
													{
														var ua = nettools.jscore.trim(input.value.substring(input.selectionStart, input.selectionEnd));
														parent.postMessage('cmd=websec_ua_update_console&ua=' + ua, '*');
													}
												);
										break;
								}
							}
						}
					}
				}
			};
			
			</script><style>

            .websec_ua .ip input[type='text'] {
				width:115px;
			}
			
			.websec_ua .ua input[type='text'] {
				width:700px;
			}
			
			.websec_ua .httpcode{
				background-color:#FAFAFA;
				font-weight:bold;
				color:dimgray;
			}

			</style><?php		
	}
	

	// affichage
	protected function _renderBody()
	{
		echo '<table cellpadding="0" cellspacing="0" border="0" class="webadminList websec_ua" onclick="return webadmin.websec.click(event);">';
		
		// retraiter les lignes
		foreach ( $this->_body as $dataline )
		{
			$styleip = !empty($dataline['newip']) ? 'highlight2': '';
			$styleua = !empty($dataline['newua']) ? 'highlight2': '';
			$line = $dataline['line'];
			
			// retravailler mise en forme certaines colonnes
			$line[0] = '<div class="ip ' . $styleip . '"><input type="text" value="' . $line[0] . '" readonly="readonly"><input type="button" value="?"><input type="button" value="+"><input type="button" value="++"></div>';
			$line[5] = '<div class="ua ' . $styleua . '"><input type="text" value="' . $line[5] . '" readonly="readonly"><input type="button" value="?"><input type="button" value="+"><input type="button" value="++"></div>';
			$line[9] = '<div class="httpcode">' . $line[9] . '</div>';
			
			echo "<tr><td>" . implode('</td><td>', $line) . '</td></tr>';
		}

		echo "</table>";
	}
	
	
	public function run(Domain $domcfg)
	{
		$data = array('count'=>0, 'body'=>'');


		$j = $_REQUEST['j'];
		if ( $j == '0' )
			$dt = time();
		else
			$dt = strtotime("yesterday") - ((int)$j-1) * 24*60*60;


		if ( $st = Helpers\Websec::_websec_ua($domcfg, $dt, $data) )
			return $this->status(false, $st);
		else	
			return $this->status(true, "Analyse effectuée : " . $data['count'] . ' user-agents identifiés.', $data['body'], true);
	}
	
}


?>