<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Domain;
use \Ppast\Webadmin\Config\WebAdminConfig;
// clauses use]


use \Ppast\Webadmin\WebAdminWebInterface\Controller;



class Websec_analyse_j extends \Ppast\Webadmin\Commands\Base
{
	// affichage
	protected function _renderCSS_JS()
	{
		parent::_renderCSS_JS();
		
		$networktools = WebAdminConfig::$ROOT_CFG->LOG__NETWORK_TOOLS;

		// config depuis contexte controller
		$domcfg = WebAdminConfig::$DOMAINS[Controller::$CONTEXT->value];
		
?><script>

			window.webadmin = window['webadmin'] || {};
			webadmin.websec = webadmin['websec'] || {
				block : function(tag, consoleBlock)
				{
					// remonter de proche en proche jusqu'à la table : parentNode : P, puis previousSibling pour TABLE
					var table = tag.parentNode.previousSibling;
						
					// énumérer cases à cocher et définir sélection
					var cbs = table.querySelectorAll("input[type='checkbox']");
					var cbsl = cbs.length;
					var select = [];
					for ( var i = 0 ; i < cbsl ; i++ )
						if ( cbs[i].checked )
							select.push(cbs[i].value);
							
					if ( select.length )
						if ( consoleBlock )
						{
							nettools.ui.desktop.dialog.confirmPromise('Interdire cette IP sur tous les sites gérés par la console d\'administration ?'). 
								then(
									function()
									{
										parent.postMessage('cmd=websec_ip_update_console&ip=' + select.join(','), '*');
									}
								);								
						}
						else
						{
							nettools.ui.desktop.dialog.confirmPromise('Interdire toutes les IPs sélectionnées pour ce site ?'). 
								then(
									function()
									{
										nettools.jscore.SecureRequestHelper.post(
												// URL
												'<?php echo \Ppast\Webadmin\WebInterface\CoreController::$routage; ?>',

												// data
												{
													u : '<?php echo \Ppast\Webadmin\WebInterface\CoreController::$USER; ?>',
													dom : '<?php echo $domcfg->name; ?>',
													ip : select.join(','),
													cmd : 'websec_ip_update'
												}
											);
									}
								);
						}
				},
				
				
				click : function(event)
				{
					var target=nettools.jscore.getTarget(event);
					
					// déterminer origine clic
					if ( target.nodeName == 'INPUT' )
					{
						// remonter au champ de saisie
						var input = target;
						while ( input && (input.type != 'text') )
							input = input.previousSibling;


						// si IP
						if ( input && (target.parentNode.className.indexOf('ip') != -1) )
						{
							if ( target.type == 'button' )
							{
								switch ( target.value )
								{
									// si interrogation IP whois
									case '?' :
										window.open(('<? echo $networktools; ?>').replace(/%/, input.value), 'network_tools');
										break;
										
										
									// si ajout uniquement dans ce site
									case '+' :
										// autoriser l'ajout si pas encore proscrit
										if ( input.parentNode.className.indexOf('highlight2') >= 0 )
											nettools.jscore.SecureRequestHelper.post(
													// URL
													'<?php echo \Ppast\Webadmin\WebInterface\CoreController::$routage; ?>',
													
													// data
													{
														u : '<?php echo \Ppast\Webadmin\WebInterface\CoreController::$USER; ?>',
														dom : '<?php echo $domcfg->name; ?>',
														ip : input.value,
														cmd : 'websec_ip_update'
													}
												);
										break;
										
										
									// si ajout dans tous les sites gérés par la console
									case '++' :
										nettools.ui.desktop.dialog.confirmPromise('Interdire cette IP sur tous les sites gérés par la console d\'administration ?'). 
											then(
												function()
												{
													parent.postMessage('cmd=websec_ip_update_console&ip=' + input.value, '*');
												}
											);
											
										break;
								}
							}
						}
					}
				}
			};
			
			</script><style>
			
			.websec_analyse .httpcode{
				background-color:#FAFAFA;
				font-weight:bold;
				color:dimgray;
			}

			</style><?php
	}
	

	// affichage
	protected function _renderBody()
	{
		// si des IPs à traiter
		if ( count($this->_body) )
		{
			echo '<table cellpadding="0" cellspacing="0" border="0" class="webadminList websec_analyse" onclick="return webadmin.websec.click(event);">';
			
			// retraiter les lignes
			foreach ( $this->_body as $dataline )
			{
				$line = $dataline['line'];
					
				// définir style IP, selon IP déjà connue ou pas
				$style = !empty($dataline['newip']) ? 'highlight2' : '';
				
				// retravailler mise en forme certaines colonnes
				$line[0] = '<div class="ip ' . $style . '"><input type="checkbox" value="' . $line[0] . '"> <input type="text" value="' . $line[0] . '" readonly="readonly"><input type="button" value="?"><input type="button" value="+"><input type="button" value="++"></div>';
				$line[6] = '<div class="uri">' . $line[6] . '</div>';
				$line[8] = '<div class="httpcode">' . $line[8] . '</div>';
				
				echo '<tr><td>' . implode('</td><td>', $line) . "</td></tr>\n";
			}
			
			echo '</table><p style="margin-bottom:30px;"><a href="javascript:void(0)" onclick="webadmin.websec.block(this); return false;">Interdire la sélection</a> - <a href="javascript:void(0)" onclick="webadmin.websec.block(this, true); return false;">Interdire la sélection sur tous les sites</a></p>';
		}
	
	}
	
	
	public function run(Domain $domcfg)
	{
		$data = array('count'=>0, 'body'=>'');

		
		$j = $_REQUEST['j'];
		if ( $j == '0' )
			$dt = time();
		else
			$dt = strtotime("yesterday") - ((int)$j-1) * 24*60*60;


		if ( $st = Helpers\Websec::_websec_analyse($domcfg, $dt, $data) )
			return $this->status(false, $st);
		else	
			return $this->status(true, "Analyse effectuée : " . $data['count'] . ' lignes suspectes identifiées.', $data['body'], true);
	}
	
}


?>