<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Domain;
use \Ppast\Webadmin\Config\WebAdminConfig;
use \Ppast\Webadmin\Includes\Tools;
// clauses use]


use \Ppast\Webadmin\WebAdminWebInterface\Controller;



class Websec_hta_ip extends \Ppast\Webadmin\Commands\Base
{
	// affichage
	protected function _renderCSS_JS()
	{
		parent::_renderCSS_JS();

		$networktools = WebAdminConfig::$ROOT_CFG->LOG__NETWORK_TOOLS;

		// config depuis contexte controller
		$domcfg = WebAdminConfig::$DOMAINS[Controller::$CONTEXT->value];

?><script>
	
			window.webadmin = window['webadmin'] || {};
			webadmin.websec = webadmin['websec'] || {
				click : function(event)
				{
					var target=nettools.jscore.getTarget(event);
					
					// déterminer origine clic
					if ( target.nodeName == 'INPUT' )
					{
						if ( target.type == 'button' )
						{
							switch ( target.value )
							{
								// si interrogation IP whois
								case '?' :
									var ip = target.parentNode.previousSibling.innerHTML;
									window.open(('<?php echo $networktools; ?>').replace(/%/, ip), 'network_tools');
									break;
									
									
								// si ajout
								case '+' :
									var ip = target.parentNode.previousSibling.getElementsByTagName('input')[0].value;
									
									// vérifier saisie
									if ( ip && ip.match(/^(?:[0-9]+\.){3}[0-9]+$/) )
										nettools.jscore.SecureRequestHelper.post(
												// URL
												'<?php echo \Ppast\Webadmin\WebInterface\CoreController::$routage; ?>',
												
												// data
												{
													u : '<?php echo \Ppast\Webadmin\WebInterface\CoreController::$USER; ?>',
													dom : '<?php echo $domcfg->name; ?>',
													'ip' : ip,
													cmd : 'websec_ip_update'
												}
											);

									break;
									
									
								// si suppression
								case 'x' :
									var ip = target.parentNode.previousSibling.innerHTML;
										nettools.jscore.SecureRequestHelper.post(
											// URL
											'<?php echo \Ppast\Webadmin\WebInterface\CoreController::$routage; ?>',
											
											// data
											{
												u : '<?php echo \Ppast\Webadmin\WebInterface\CoreController::$USER; ?>',
												dom : '<?php echo $domcfg->name; ?>',
												'ip' : ip,
												cmd : 'websec_hta_ip_delete'
											}
										);

									break;
							}
						}
					}
				}
			};
			
			</script><style>
			
			.websec_hta_ip .ip input[type='text'] {
				width:115px;
			}

			</style><?php
	}
	

	// affichage
	protected function _renderBody()
	{
		// constituer tableau			
		echo '<table cellpadding="0" cellspacing="0" border="0" class="webadminList websec_hta_ip" onclick="return webadmin.websec.click(event);">';
		echo "<tr class=\"newline\"><td class=\"ip\"><input type=\"text\" style=\"width:100%\"></td><td class=\"ip\"><input type=\"button\" value=\"+\"></td></tr>";

		// pour toutes les IPs proscrites
		foreach ( $this->_body as $ip )
			echo "<tr><td class=\"ip\">$ip</td><td class=\"ip\"><input type=\"button\" value=\"?\"><input type=\"button\" value=\"x\"></td></tr>\n";
			
		echo '</table>';
	}
	
	
	public function run(Domain $domcfg)
	{
		$data = array('count'=>0, 'body'=>'');


		// lire le fichier HTACCESS pour faire un cache des IP déjà proscrites
		$hta = file_get_contents(Tools::ensureTrailingSlash(WebAdminConfig::$ROOT) . '.htaccess');
		$hta = Helpers\Websec::_websec_analyse_ips($domcfg, $hta);
		if ( $hta === FALSE )
			return $this->status(false, 'Impossible d\'identifier les IPs proscrites.');


		if ( count($hta) )
		{
			// renvoyer tableau formaté
			$data['body'] = $hta;
			$data['count'] = count($hta);
		}

		return $this->status(true, "Extraction .htaccess ci-dessous : " . $data['count'] . ' IPs proscrites.', $data['body'], true);
	}
	
}


?>