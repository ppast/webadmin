<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands\Plugins;



// [clauses use
use \Ppast\Webadmin\Config\Profile;
use \Ppast\Webadmin\Config\WebAdminConfig;
use \Ppast\Webadmin\Includes\Tools;
// clauses use]



class Phpunit extends \Ppast\Webadmin\Commands\Base
{
	public function run(Profile $appcfg, \Ppast\Webadmin\Config\Plugin $plugincfg, $cmd)
	{
		return call_user_func(array($this, $cmd), $appcfg, $plugincfg);
	}


	// exécuter les tests	
	protected function tests(Profile $appcfg, \Ppast\Webadmin\Config\Plugin $plugincfg)
	{
		$root = Tools::ensureTrailingSlash(WebAdminConfig::$ROOT);
		

		// chemin exécutable php
		$php = $plugincfg->PHPBIN;
		$libcphp = Tools::ensureTrailingSlash($plugincfg->PHPUNIT_COMPOSER_PROJECT_FOLDER);
		$phpunit_options = $plugincfg->PHPUNIT_OPTIONS ? $plugincfg->PHPUNIT_OPTIONS : '';


		// chemins racine et phpunit
		$phpunit = $root . $libcphp . "vendor/bin/phpunit";
		$lib = $root . $plugincfg->PHPUNIT_FOLDER;


		$cmd = "cd $lib ; $php $phpunit $phpunit_options 2>&1";
		$ret = shell_exec($cmd);
		$ret = htmlentities($ret);

		return $this->status(true, 'Tests exécutés', '<div class="console" style="white-space:pre;">' . $ret . '</div>', true);
	}
	
}
?>