<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands\Plugins;



// [clauses use
use \Ppast\Webadmin\Config\Profile;
use \Ppast\Webadmin\Config\WebAdminConfig;
use \Ppast\Webadmin\Includes\Tools;
// clauses use]



class Merge extends \Ppast\Webadmin\Commands\Base
{
	public function run(Profile $appcfg, \Ppast\Webadmin\Config\Plugin $plugincfg, $cmd)
	{
		// si méthode appelée concerne l'optimisation
		if ( strpos($cmd, 'merge_') === 0 )
		{
			return $this->_merge(strtoupper(substr(strstr($cmd, '_'), 1)), $appcfg, $plugincfg);
		}
	}
	
	
	public function _merge($merge, Profile $appcfg, \Ppast\Webadmin\Config\Plugin $plugincfg)
	{
		// préparer fichier fusionné
		$output = Tools::ensureTrailingSlash(WebAdminConfig::$ROOT) . $plugincfg->{'CFG_MERGE_' . $merge . '_OUTPUT'};
		$f = fopen($output, 'w');
		
		// pour toutes les sources énumérées
		foreach ( $plugincfg->{'CFG_MERGE_' . $merge . '_PARTS'} as $part )
			if ( file_exists(Tools::ensureTrailingSlash(WebAdminConfig::$ROOT) . $part) )
				fwrite($f, file_get_contents(Tools::ensureTrailingSlash(WebAdminConfig::$ROOT) . $part));
			else
				return $this->status(false, 'Erreur pour obtenir la source ' . $part . '');
				
		fclose($f);


		// déterminer type d'optimisation
		$kind = $plugincfg->{'CFG_MERGE_' . $merge . '_KIND'};
		if ( !$kind )
			return $this->status(false, 'Impossible de déterminer le type d\'optimisation');

		
		if ( $kind == 'js' )
		{
			// déterminer version ecmascript
			$language_in = $plugincfg->{'CFG_MERGE_' . $merge . '_ECMASCRIPT'};
			if ( !$language_in )
				$language_in = WebAdminConfig::$ROOT_USER_CFG->ECMASCRIPT_DEFAULT;
		}
		else
			$language_in = null;


		// optimiser
		$st = Tools::minify($output, str_replace(array('.js', '.css'), array('.min.js', '.min.css'), $output), $kind, $language_in);
		if ( is_string($st) )
			// si erreur pour minifier avec google closure ET librairie php
			return $this->status(false, 'Erreur à l\'optimisation : ' . $st);
		else
			return $this->status(true, 'Fusion et optimisation effectuées (' . $st[0] . ').', isset($st[1]) ? $st[1] : NULL);
	}
}

?>