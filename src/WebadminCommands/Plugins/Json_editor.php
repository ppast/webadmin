<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands\Plugins;



// [clauses use
use \Ppast\Webadmin\Config\Profile;

// clauses use]



class Json_editor extends \Ppast\Webadmin\Commands\Base
{
	public function run(Profile $appcfg, \Ppast\Webadmin\Config\Plugin $plugincfg, $cmd)
	{
		// cmd de la forme "json_cmd__file.ext"
        $cmd = explode('__', $cmd);
        $file = $cmd[1];
        $cmd = $cmd[0];
        
        return call_user_func(array($this, $cmd), $appcfg, $plugincfg, $file);
        
	}


    // écrire le fichier demandé
	public function json_save(Profile $appcfg, \Ppast\Webadmin\Config\Plugin $plugincfg, $file)
    {
        // écrire fichier
        $root = $_SERVER['DOCUMENT_ROOT'];
        $f = fopen($root . $plugincfg->ACTIONS["json_edit__$file"], 'w');
        fwrite($f, str_replace(array('&lt;','&gt;'), array('<', '>'), $_REQUEST['json']));
        fclose($f);

        return $this->status(true, 'Fichier JSON enregistré');		
    }
    
    
    // lire le fichier demandé
	public function json_edit(Profile $appcfg, \Ppast\Webadmin\Config\Plugin $plugincfg, $file)
    {
        // lire fichier
        $root = $_SERVER['DOCUMENT_ROOT'];
        $f = htmlentities(file_get_contents($root . $plugincfg->ACTIONS["json_edit__$file"]));
        
        // user et routage
        $user = \Ppast\Webadmin\WebInterface\CoreController::$USER;
        $routage = \Ppast\Webadmin\WebInterface\CoreController::$routage;
        
        // js
        $js = <<<JS
<script>

window.json_editor = {

    // enregistrer les modifications
    submit : function()
    {
        var json = document.getElementById('json_editor');
        
        try
        {
            JSON.parse(json.value);
        }
        catch (e)
        {
            nettools.ui.desktop.dialog.notify('La syntaxe JSON est invalide :' + e);
            return;
        }
    
    
        // sauvegarder
		nettools.jscore.SecureRequestHelper.post(

                // URL
                '$routage',

                // data
                {
                    u : '$user',
                    app : '$appcfg->name',
                    cmd : 'json_save__$file',
                    plugin : '$plugincfg->name',
                    json : json.value.replace(/</g, '&lt;').replace(/>/g, '&gt;')
                }
            );
                
    }

}

</script>
JS;
        
        $html = <<<HTML
<textarea id="json_editor" style="background-color:black; color:#FFF; font-family:Courier; width:100%; height:25em; font-size:1em;" spellcheck="false">
$f
</textarea>
<input type="button" value="Enregistrer les modifications" onclick="json_editor.submit(); return false;">
HTML;
        
        return $this->status(true, 'Fichier JSON lu', $js . $html, true);		
    }
}
?>