<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Domain;
use \Ppast\Webadmin\Config\WebAdminConfig;
use \Ppast\Webadmin\Includes\Tools;
// clauses use]


use \Ppast\Webadmin\WebAdminWebInterface\Controller;



class Websec_hta_ua extends \Ppast\Webadmin\Commands\Base
{
	// affichage
	protected function _renderCSS_JS()
	{
		parent::_renderCSS_JS();

		$gsearch_ua = WebAdminConfig::$ROOT_CFG->LOG__UA;

		// config depuis contexte controller
		$domcfg = WebAdminConfig::$DOMAINS[Controller::$CONTEXT->value];


?><script>
	
			window.webadmin = window['webadmin'] || {};
			webadmin.websec = webadmin['websec'] || {
				click : function(event)
				{
					var target=nettools.jscore.getTarget(event);
					
					// déterminer origine clic
					if ( target.nodeName == 'INPUT' )
					{
						// remonter à la valeur
						var ua = target.parentNode.previousSibling.innerHTML;
	
	
						if ( target.type == 'button' )
						{
							switch ( target.value )
							{
								// si interrogation user-agent
								case '?' :
									window.open(('<?php echo $gsearch_ua; ?>').replace(/%/, ua + '+user+agent'), 'ua');
									break;
									
									
								// si ajout
								case '+' :
									var ua = nettools.jscore.trim(target.parentNode.previousSibling.getElementsByTagName('input')[0].value);
									
									// vérifier saisie
									if ( ua )
										nettools.jscore.SecureRequestHelper.post(
												// URL
												'<?php echo \Ppast\Webadmin\WebInterface\CoreController::$routage; ?>',
												
												// data
												{
													u : '<?php echo \Ppast\Webadmin\WebInterface\CoreController::$USER; ?>',
													dom : '<?php echo $domcfg->name; ?>',
													'ua' : ua,
													cmd : 'websec_ua_update'
												}
											);

									break;
									
									
								// si suppression
								case 'x' :
										nettools.jscore.SecureRequestHelper.post(
											// URL
											'<?php echo \Ppast\Webadmin\WebInterface\CoreController::$routage; ?>',
											
											// data
											{
												u : '<?php echo \Ppast\Webadmin\WebInterface\CoreController::$USER; ?>',
												dom : '<?php echo $domcfg->name; ?>',
												'ua' : ua,
												cmd : 'websec_hta_ua_delete'
											}
										);

									break;
							}
						}
					}
				}
			};
			
			</script><style>
		
			.websec_hta_ua .ua input[type='text'] {
				width:115px;
			}
				
			</style><?php
	}


	// affichage
	protected function _renderBody()
	{
		echo '<table cellpadding="0" cellspacing="0" border="0" class="webadminList websec_hta_ua" onclick="return webadmin.websec.click(event);">';
		echo "<tr class=\"newline\"><td class=\"ua\"><input type=\"text\" style=\"width:100%\"></td><td class=\"ua\"><input type=\"button\" value=\"+\"></td></tr>";

		// pour toutes les user-agents proscrits
		foreach ( $this->_body as $ua )
			echo "<tr><td class=\"ua\">$ua</td><td><input type=\"button\" value=\"?\"><input type=\"button\" value=\"x\"></td></tr>\n";

		echo '</table>';
	}
	

	public function run(Domain $domcfg)
	{
		$data = array('count'=>0, 'body'=>'');
		

		// lire le fichier HTACCESS pour faire un cache des user-agents déjà proscrits
		$hta = file_get_contents(Tools::ensureTrailingSlash(WebAdminConfig::$ROOT) . '.htaccess');
		$hta = Helpers\Websec::_websec_ua_useragents($domcfg, $hta);
		if ( $hta === FALSE )
			return 'Impossible d\'identifier les user-agents proscrits.';
			
		if ( count($hta) )
		{

			// renvoyer tableau formaté
			$data['body'] = $hta;
			$data['count'] = count($hta);
		}
		
		
		
		return $this->status(true, "Extraction .htaccess ci-dessous : " . $data['count'] . ' user-agents proscrits.', $data['body'], true);
	}
	
}


?>