<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Domain;
use \Ppast\Webadmin\Config\WebAdminConfig;
// clauses use]


use \Ppast\Webadmin\WebAdminWebInterface\Controller;



class Download_dump_web extends \Ppast\Webadmin\Commands\Base
{
	public function run(Domain $domcfg)
	{
		$f = WebAdminConfig::$ROOT . '_dump_' . $domcfg->name . '.zip';
		
		if ( file_exists($f) )
		{
			header('Location: /_dump_' . $domcfg->name . '.zip');
			die();
		}
		else
			return $this->status(false, 'La sauvegarde n\'existe pas (encore).');
	}
	
}


?>