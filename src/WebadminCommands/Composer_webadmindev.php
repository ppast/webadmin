<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Domain;
use \Ppast\Webadmin\Config\WebAdminConfig;
use \Ppast\Webadmin\Includes\Tools;
// clauses use]



class Composer_webadmindev extends \Ppast\Webadmin\Commands\Base
{
	public function run(Domain $domcfg)
	{
		$packages = array();
		
		
		// pour tous les dossiers webadmin clonables en dev
		foreach ( WebAdminConfig::$ROOT_CFG->COMPOSER_WEBADMIN_PACKAGES as $package )
		{
			// commencer par clôner les dossiers webadmin en DEV
			$source = Tools::ensureTrailingSlash(Tools::ensureTrailingSlash(WebAdminConfig::$ROOT) . $_REQUEST['libcsource']) . "vendor/$package/";
			$devc = Tools::ensureTrailingSlash(Tools::ensureTrailingSlash(WebAdminConfig::$ROOT) . $domcfg->COMPOSER_DEV_PATH) . $package . '/';
			
			// vérifier que le source existe ; si n'existe pas, passer au package suivant (certains sites n'ont pas tous les packages clonables, comme webadmin_console)
			if ( !file_exists($source) )
				continue;
				
			// package pris en considération
			$packages[] = $package;
			

			// vérifier que le dossier cible n'existe pas
			if ( file_exists($devc) )
				return $this->status(false, "Le dossier de développement cible '$devc' existe déjà ; clônage webadmin abandonné");
	
	
			// créer le dossier cible ; copier les fichiers
			$script = "mkdir -p {$devc} ; cp -r {$source}* $devc";
			$ret = shell_exec("$script 2>&1");
	
			if ( $ret && trim($ret) )
				return $this->status(false, "Erreur au clônage du package '$devc'.", Tools::consoleOutput($ret), true);
		}
		
		
		// créer maintenant un dossier libc ;  vérifier que le dossier cible n'existe pas
		$libc = Tools::ensureTrailingSlash(Tools::ensureTrailingSlash(WebAdminConfig::$ROOT) . $_REQUEST['libcdest']);
		if ( file_exists($libc) )
			return $this->status(false, "Le dossier libc cible '$libc' existe déjà ; clônage webadmin abandonné");
		
		mkdir($libc);

		
		// préparer un fichier json 
/*
    "require": {
        "ppast/webadmin_core": "@dev",
        "ppast/webadmin": "@dev"
    },

   "repositories": [
      {
          "type": "path",
          "url": "../devc/ppast/webadmin_core"
      },
      {
          "type": "path",
          "url": "../devc/ppast/webadmin"
      }
*/ 

		// lire le json par défaut et le décoder en objet
		$jsono = (object) array(
                "name"=>"vendor/name", 
                "description"=>"composer webadmin dev",
            );

		
		// pour tous les dossiers webadmin clonables en dev
		$jsono->repositories = array();
		$jsono->{'require'} = array();
		foreach ( $packages as $package )
		{
			$jsono->repositories[] = (object) array('type'=>'path', 'url'=>'../' . Tools::ensureTrailingSlash($domcfg->COMPOSER_DEV_PATH) . $package);
			$jsono->{'require'}{$package} = '@dev';
		}

		$json = json_encode($jsono, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);	
		
    
        
        try
        {
            // installer composer ; si pb exception
            $ret = \Ppast\Webadmin\WebadminCommands\Helpers\ComposerInterface::get($_REQUEST['libcdest'])->setup($json);

            // exécuter update
            $ret = \Ppast\Webadmin\WebadminCommands\Helpers\ComposerInterface::get($_REQUEST['libcdest'])->update();
            
            // vérifier update ok pour tous les packages 
            if ( mb_substr_count($ret, WebAdminConfig::$ROOT_CFG->COMPOSER_REQUIRE_DEV_OK) === count($packages) )
                return $this->status(true, 'Opération terminée ; <u>RAJOUTER le dossier \'' . $_REQUEST['libcdest'] . '\' dans la configuration Composer du domaine et modifier l\'include vendor</u>');
            else
                return $this->status(false, "Erreur à la mise à jour initiale Composer pour '$devc'.", Tools::consoleOutput($ret), true);
        }
        catch(\Throwable $e)
        {
            return $this->status(false, 'Une exception est survenue : ', Tools::consoleOutput($e->getMessage()), true);
        }
        
        
		
	}
	
}


?>