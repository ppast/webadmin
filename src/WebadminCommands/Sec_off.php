<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Profile;
// clauses use]



class Sec_off extends \Ppast\Webadmin\Commands\Base
{
	public function run(Profile $appcfg)
	{
		if ( $st = Helpers\Opt::_secOnOff(false, $appcfg) )
			return $this->status(false, $st);
		
		return $this->status(true, "Verrouillage du profil désactivé.");
	}
	
}


?>