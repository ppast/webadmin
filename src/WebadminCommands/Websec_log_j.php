<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Domain;
// clauses use]



class Websec_log_j extends \Ppast\Webadmin\Commands\Base
{
	public function run(Domain $domcfg)
	{
		$j = $_REQUEST['j'];
		if ( $j == '0' )
			$dt = time();
		else
			$dt = strtotime("yesterday") - ((int)$j-1) * 24*60*60;



		// obtenir log
		$output = Helpers\Websec::_websec_get_log($domcfg, $dt);
		
		// si erreur, on a un tableau (statut=false, message)
		if ( is_array($output) && !$output['status'] )
			return $this->status(false, $output['message']);
// 77.73.2.192 www.assistance-multimedia63.fr - [26/May/2014:05:30:54 +0200] "GET //wp-login.php HTTP/1.1" 403 221 "-" "Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm"
			
		// décoder le fichier GZ (si nécessaire ; les logs du jour OSL ne sont pas compressées)
		if ( date("Ymd", $dt) != date("Ymd", time()) )
			$output = gzdecode($output);
				
		header("Content-Type: text/plain");
		header('Content-Disposition: attachment; filename="log_' . date("Y-m-d", $dt) . '.txt"');
		header("Expires: 0");
		header("Cache-Control: no-cache, must-revalidate");
		header("Pragma: no-cache"); 
		echo $output;
		die();
	}
	
}


?>