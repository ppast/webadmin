<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Profile;
use \Ppast\Webadmin\Includes\Tools;
// clauses use]



class Manifest_config extends \Ppast\Webadmin\Commands\Base
{
	public function run(Profile $appcfg)
	{
		if ( $st = Tools::writeManifestConfig($_REQUEST['config'], $appcfg) )
			return $this->status(false, $st);
		else
			return $this->status(true, 'Paramètres de configuration enregistrés');
	}
	
}


?>