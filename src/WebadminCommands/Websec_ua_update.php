<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Domain;
// clauses use]



class Websec_ua_update extends \Ppast\Webadmin\Commands\Base
{
	public function run(Domain $domcfg)
	{
		if ( $st = Helpers\Websec::_websec_ua_update($domcfg, $_REQUEST['ua']) )
			return $this->status(false, $st);
		else	
			return $this->status(true, "Mise à jour des user-agents effectuée.");
	}
	
}


?>