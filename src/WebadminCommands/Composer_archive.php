<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Domain;
use \Ppast\Webadmin\Includes\Tools;
// clauses use]



class Composer_archive extends \Ppast\Webadmin\Commands\Base
{
	public function run(Domain $domcfg)
	{
        try
        {
            $fname = str_replace('/','!',$_REQUEST['package']) . '!' . uniqid() . '.zipdev';
            $ret = \Ppast\Webadmin\WebadminCommands\Helpers\ComposerInterface::get()->package_archive($_REQUEST['package'], 'zip', true, $fname);
            return $this->status(true, 'Show exécuté', Tools::consoleOutput($ret), true);
        }
        catch(\Throwable $e)
        {
            return $this->status(false, 'Une exception est survenue : ', Tools::consoleOutput($e->getMessage()), true);
        }

	}
	
}


?>