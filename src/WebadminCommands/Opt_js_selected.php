<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Profile;
// clauses use]



class Opt_js_selected extends \Ppast\Webadmin\Commands\Base
{
	public function run(Profile $appcfg)
	{
		// fichiers à traiter
		$files = explode(';', $_REQUEST['files']);
		
		
		// puis on constitue des batchs
		$data2 = array('batches' => NULL);
		if ( $st = Helpers\Opt::_minify_js_pass2($files, $data2, $appcfg) )
			return $this->status(false, $st);
		
		// puis on envoie la requete
		$data3 = array('count' => 0, 'feedback' => '', 'mode_degrade'=>false);
		if ( $st = Helpers\Opt::_minify_js_pass3($data2['batches'], $data3, $appcfg) )
			return $this->status(false, $st);
		
		// consigner nouvelle version
		return $this->status(true, "Traitement sélectif effectué : " . $data3['count'] . ' fichier(s) optimisé(s)' . ($data3['mode_degrade'] ? ' <u>en mode dégradé</u>.':'.'), $data3['feedback']);
	}
	
}


?>