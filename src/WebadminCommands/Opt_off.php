<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Profile;
// clauses use]



class Opt_off extends \Ppast\Webadmin\Commands\Base
{
	public function run(Profile $appcfg)
	{
		if ( $st = Helpers\Opt::_optOnOff(false, $appcfg) )
			return $this->status(false, $st);
		
		return $this->status(true, "Optimisation site désactivée.");
	}
	
}


?>