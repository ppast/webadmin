<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Domain;
use \Ppast\Webadmin\Config\WebAdminConfig;
// clauses use]


use \Ppast\Webadmin\WebAdminWebInterface\Controller;



class Delete_dump_web extends \Ppast\Webadmin\Commands\Base
{
	public function run(Domain $domcfg)
	{
		$f = WebAdminConfig::$ROOT . '_dump_' . $domcfg->name . '.zip';
		
		if ( file_exists($f) )
			unlink($f);

		
		return $this->status(true, 'Traitement effectué');
	}
	
}


?>