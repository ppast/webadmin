<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use

// clauses use]



class Users_delete extends \Ppast\Webadmin\Commands\Base
{
	public function run()
	{
		$user = $_REQUEST['user'];

		// update user
		\Ppast\Webadmin\Config\Users::usersProvider()->removeUser($user);


		return $this->status(true, "Utilisateur '$user' supprimé.");
	}
	
}


?>