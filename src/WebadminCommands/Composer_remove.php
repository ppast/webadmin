<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Domain;
use \Ppast\Webadmin\Config\WebAdminConfig;
use \Ppast\Webadmin\Includes\Tools;
// clauses use]



class Composer_remove extends \Ppast\Webadmin\Commands\Base
{
	public function run(Domain $domcfg)
	{
		// lire composer.json pour déterminer version actuelle (x.y.z ou @dev)
		$libc = Tools::ensureTrailingSlash(Tools::ensureTrailingSlash(WebAdminConfig::$ROOT) . $_REQUEST['libc']);
		$json = json_decode(file_get_contents($libc . 'composer.json'));

		// version du package à enlever
		$pver = $json->{'require'}->{$_REQUEST['package']};


        try
        {
            $ret = \Ppast\Webadmin\WebadminCommands\Helpers\ComposerInterface::get()->package_remove($_REQUEST['package']);
            if ( mb_strpos($ret, WebAdminConfig::$ROOT_CFG->COMPOSER_REMOVE_OK) === FALSE )
                return $this->status(false, 'Problème à la suppression du package dans Composer.', Tools::consoleOutput($ret), true);
            
            
            // si package DEV
            if ( $pver == '@dev' )
            {
                // supprimer repository
                $repo = '../' . Tools::ensureTrailingSlash($domcfg->COMPOSER_DEV_PATH) . $_REQUEST['package'] . '/';
                \Ppast\Webadmin\WebadminCommands\Helpers\ComposerInterface::get()->repository_remove($repo);
            }

            return $this->status(true, 'Remove exécuté', Tools::consoleOutput($ret), true);
        }
        catch(\Throwable $e)
        {
            return $this->status(false, 'Une exception est survenue : ', Tools::consoleOutput($e->getMessage()), true);
        }
	}
	
}


?>