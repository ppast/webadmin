<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Profile;
use \Ppast\Webadmin\Config\WebAdminConfig;
use \Ppast\Webadmin\Includes\Tools;
// clauses use]



class Del_opt_css extends \Ppast\Webadmin\Commands\Base
{
	// supprimer les fichiers CSS optmisés
	public function run(Profile $appcfg)
	{
		$data = array('count' => 0, 'feedback' => '');
		if ( $st = Helpers\Opt::_folder(Tools::ensureTrailingSlash(WebAdminConfig::$ROOT) . $appcfg->OPT__ROOT, '*.min.css', array('\Ppast\Webadmin\WebadminCommands\Helpers\Opt', '_del_opt'), $data, $appcfg) )
			return $this->status(false, $st);
		else
			return $this->status(true, "Traitement effectué : " . $data['count'] . ' fichier(s) optimisé(s) effacé(s).', $data['feedback']);
	}
}


?>