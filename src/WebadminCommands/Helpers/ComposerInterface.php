<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands\Helpers;



// [clauses use
use \Ppast\Webadmin\Config\WebAdminConfig;
use \Ppast\Webadmin\Includes\Tools;
// clauses use]



// classe
class ComposerInterface
{
    // cache composer
    protected static $_cache = array();
    
    
    // créer l'interface composer
    protected static function create($libc)
    {
        $config = \Nettools\ComposerInterface\Config::fromJSON(__DIR__ . '/composer.config.json');
        $config->composer_home = dirname(rtrim($_SERVER['DOCUMENT_ROOT'], '/'));

        // create interface and set the composer project to be in folder PROJECT
        return new \Nettools\ComposerInterface\ComposerInterface($config, $libc);
    }
    
    
    // obtenir interface composer
    static function get($libc = NULL)
    {
        if ( is_null($libc) )
            $libc = $_REQUEST['libc'];

        $libc = Tools::ensureTrailingSlash(WebAdminConfig::$ROOT) . trim($libc, '/') . '/';
                
        // regarder dans le cache
		$md5 = md5($libc);
        if ( array_key_exists($md5, self::$_cache) )
			return self::$_cache[$md5];
		else
			return self::$_cache[$md5] = self::create($libc);
    }
}

?>