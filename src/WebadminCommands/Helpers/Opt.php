<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands\Helpers;



// [clauses use
use \Ppast\Webadmin\Config\Profile;
use \Ppast\Webadmin\Config\Core;
use \Ppast\Webadmin\Config\WebAdminConfig;
use \Ppast\Webadmin\Includes\Tools;
// clauses use]



// classe
class Opt 
{


	// ce dossier est-il en lecture/écriture (et donc exclu de la sécurisation) ?
	static function _isFolderRW($folder, Profile $appcfg)
	{
		// extraire la partie relative /desk/mobilite/
		$folder = str_replace(Tools::ensureNoTrailingSlash(Tools::ensureNoTrailingSlash(WebAdminConfig::$ROOT) . $appcfg->SEC__ROOT), '', $folder);
		return in_array($folder, $appcfg->SEC__RW_FOLDERS);
	}
	
	
	// optimiser un dossier (récursivité)
	static function _folderSecOnOff($folder, $chmod_f, $chmod_d, Profile $appcfg, $firstlevel = true)
	{
		// traiter par récursivité les dossiers et fichiers ; on ne considère, au premier niveau, que les dossiers énumérés dans la config OPT
		$files = glob(Tools::ensureTrailingSlash(str_replace('//', '/', $folder)) . '{,.}*', GLOB_BRACE); // recherche aussi fichiers cachés ".xxx" et fichier "xxx"
		if ( is_array($files) )
		{
			foreach ( $files as $f )
			{
				// ignorer dossiers courant et précédent
				if ( substr($f, -1) == '.' )
					continue;
				
				// tester si dossier pour traiter exploration récursive
				if ( is_dir($f) )
				{
					// si dossier peut être considéré (soit premier niveau et le dossier est considéré s'il est indiqué dans config, soit autre niveau et on traite)
					if ( !$firstlevel || self::_isFolderIncluded($f, $appcfg->SEC__FOLDERS) )
						// si dossier n'est pas exclu ; si dossier exclu, on ne change pas son chmod
						if ( !self::_isFolderRW($f, $appcfg) )
						{
							// traiter par récursivité ses sous-dossiers
							if ( $st = self::_folderSecOnOff($f, $chmod_f, $chmod_d, $appcfg, false) )
								return $st;
								
							// appliquer chmod sur ce dossier
							chmod($f, $chmod_d);
						}
				}
				
				// si fichier, et pas premier niveau, changer son chmod
				elseif ( !$firstlevel )
				{
					chmod($f, $chmod_f);
				}
			}
		}
		else
			return 'Erreur GLOB pour inventaire dossiers.';

		return NULL;
	}
	
	
	// verrouiller/déverrouiller le site selon un script donné
	static function _secOnOffScript($script)
	{
		if ( !$script )
			return "Script Shell inconnu";

		// exécuter shell. Un debug a permis de voir qu'il fallait indiquer le dossier courant ; les erreurs doivent être redirigées sur la console (2>&1)
		if ( $st = shell_exec(". " . Core::$DATA_ROOT . "scripts_profiles/$script 2>&1") )
//		if ( $st = shell_exec(". ./data/scripts/$script 2>&1") )
			return "Erreur Shell : $st";
		else
			return NULL;
	}
	
	
	// verrouiller/déverrouiller le profil
	static function _secOnOff($on, Profile $appcfg)
	{
		// déterminer méthode de sécurisation
		switch ( $appcfg->SEC__METHOD )
		{
			//si par PHP (opur gérer exclusions)
			case 'intelligent' :
				// protéger/déprotéger les dossiers gérés par webadmin, comprenant des exclusions
				if ( $on )
					return self::_folderSecOnOff(WebAdminConfig::$ROOT . $appcfg->SEC__ROOT, 0404, 0505, $appcfg);
				else
					return self::_folderSecOnOff(WebAdminConfig::$ROOT . $appcfg->SEC__ROOT, 0604, 0705, $appcfg);
				
	
			// si par shell et scripts
			case 'scripts':
				return self::_secOnOffScript($appcfg->SEC__SCRIPTS[$on ? 'on':'off']);
			
			
			// méthode inconnue
			default:
				return "Méthode de sécurisation inconnue";
		}
	}
	
	
	// dossier est-il inclu dans liste ?
	static function _isFolderIncluded($folder, $folderlist)
	{
//		if ( ($folder = preg_match('~/([a-zA-Z0-9_-]+)$~', $folder, $regs)) && (count($regs) == 2) ) 
        
        $folder = substr(strrchr($folder, '/'), 1);
		return in_array($folder, $folderlist);
	}
	

	// optimiser un dossier (récursivité)
	static function _folder($folder, $mask, $fun, &$fundata, Profile $appcfg, $firstlevel = true, $excludefirstlevel = true)
	{
		// traiter par récursivité les dossiers
		$dirs = glob(Tools::ensureTrailingSlash(str_replace('//', '/', $folder)) . '*', GLOB_ONLYDIR);
		if ( is_array($dirs) )
		{
			foreach ( $dirs as $dir )
				if ( !$firstlevel || self::_isFolderIncluded($dir, $appcfg->OPT__INCLUDE_FOLDERS) )
					if ( $st = self::_folder($dir, $mask, $fun, $fundata, $appcfg, false) )
						return $st;
		}
		else
			return 'Erreur GLOB pour inventaire dossiers.';
	
	
		// traiter les fichiers des dossiers, sauf si on vient d'appeler la fonction ; par construction, on ne traite que les sous-dossiers grâce à _isFolderIncluded 
		if ( $excludefirstlevel && $firstlevel )
			return NULL;
		
		$files = glob(Tools::ensureTrailingSlash(str_replace('//', '/', $folder)) . $mask);
		if ( is_array($files) )
			foreach ( $files as $f )
				if ( $st = call_user_func_array($fun, array($f, &$fundata, $appcfg)) )
					return $st;
					
		return NULL;
	}
	
	
	// compacter un fichier CSS
	static function _minify_css($f, &$fundata, Profile $appcfg)
	{
		// si glob a fourni un fichier déjà minifié, ne rien faire (car glob utilise *.css)
		if ( strpos($f, '.min.css') > 0 )
			return null;
		
			
		// minifier CSS
		$st = Tools::minify($f, str_replace('.css', ".min.css", $f), 'css');
		
		// si erreur d'optimisation
		if ( is_string($st) )
			return $st;
			
		// comptabiliser ce traitement
		$fundata['count']++;
		$fundata['feedback'] .= $st[0] . "\n";
		return NULL;
	}
	

	// compacter un fichier JS : cataloguer les fichiers à traiter
	static function _minify_js($f, &$fundata)
	{
		// si glob a fourni un fichier déjà minifié, ne rien faire (car glob utilise *.js)
		if ( strpos($f, '.min.js') > 0 )
			return null;

		
		// convertir le fichier /home/www/dir1/dir2/f.js en URL dir1/dir2/f.js
		$fundata['files'][] = str_replace(Tools::ensureTrailingSlash(WebAdminConfig::$ROOT), '', $f);
		
		return NULL;
	}
	

	// compacter un fichier JS : faire des batchs
	static function _minify_js_pass2($files, &$fundata, Profile $appcfg)
	{
		$batchs = array();
		$urls = array();
		
		// traiter tous les fichiers et les scinder en batchs
		for ( $i = 0 ; $i < count($files) ; $i++ )
		{
			// si atteint le nb d'url par batch, constituer ce lot
			if ( count($urls) == $appcfg->OPT_JS__BATCH )
			{
				$batchs[] = $urls;
				$urls = array();
			}
			
			// cas général, maxi batch pas encore atteint
			$urls[] = $files[$i];
		}
		
		// quand on arrive ici, constituer le dernier batch
		if ( count($urls) )
			$batchs[] = $urls;
		
		// renvoyer batchs
		$fundata['batches'] = $batchs;
			
		return NULL;
	}
	
	
	// compacter un fichier JS
	static function _minify_js_pass3($batches, &$fundata, Profile $appcfg)
	{
		// créer la connexion et la réutiliser ensuite
		$c = curl_init(); 
		$fundata['mode_degrade'] = false;
		
		// prendre version ecmascript soit depuis config profil soit depuis config root user
		$language_in = $appcfg->OPT_JS__IN;
		
		$language_out = $appcfg->OPT_JS__OUT;
		if ( !$language_out )
			$language_out = WebAdminConfig::$ROOT_USER_CFG->OPT_JS__OUT;
		if ( !$language_out )
			$language_out = 'ECMASCRIPT_2017';

		
		// traiter tous les batchs
		$files = [];
		foreach ( $batches as $batch )
		{
			// constituer URL
			foreach( $batch as $file )
				$files[] = "code_url=" . urlencode(Tools::ensureTrailingSlash($appcfg->OPT_JS__WWW) . $file . "?rnd=" . time());
        
      		$url = implode("&", $files);
			
			curl_setopt($c, CURLOPT_URL, 'https://closure-compiler.appspot.com/compile');
			
			// on fait une requête POST
			curl_setopt($c, CURLOPT_POST, true);
			
			// on veut récupérer les données renvoyées par le serveur
			curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
			
			// suivi
			curl_setopt($c, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($c, CURLOPT_POSTREDIR, CURL_REDIR_POST_ALL);
			
			// les champs
			curl_setopt($c, CURLOPT_POSTFIELDS, $url . 
							implode('&', 
									array_filter(
											array(
												'charset=UTF-8',
												'compilation_level=SIMPLE_OPTIMIZATIONS',
												'output_format=json',
												'output_info=compiled_code',
												'output_info=errors',
												'formatting=print_input_delimiter',
												$language_out ? "language_out=$language_out" : NULL,
												$language_in ? "language=$language_in" : NULL
											)
									)
								)
							);

			
			// timeout
			curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 5);
			curl_setopt($c, CURLOPT_TIMEOUT, 30);
			
			
			// exécuter la requête
			$output = curl_exec ($c);
			
			// obtenir un code d'erreur HTTP
			$i = curl_getinfo($c,  CURLINFO_HTTP_CODE);
			
			// HTTP 200 : ok
			if ( $i != 200 ) 
			{
				$ret = "Erreur HTTP $i au traitement du batch comprenant '" . implode("\n", $batch) . "' : " . curl_error($c);
				curl_close($c);
				return $ret;
			}
			else
			{
				// décoder réponse json
				$json = json_decode($output, true);
				
				// si erreurs (erreur parser)
				if ( array_key_exists('errors', $json) && is_array($json['errors']) && count($json['errors']) )
				{
					$msg = 'Erreur(s) optimisation : ';
					foreach ( $json['errors'] as $err )
						$msg .= 'Erreur ' . $err['error'] . '  -  fichier ' . $err['file'] . '  -  ligne ' . $err['lineno'] . '  :  ' . $err['line'] . "\n";
					
					curl_close($c);	
					return $msg;
				}
				
				
				// si erreur du service
				if ( array_key_exists('serverErrors', $json) && is_array($json['serverErrors']) && count($json['serverErrors']) )
				{
					$err = $json['serverErrors'][0];
					$fundata['mode_degrade'] = true;
					
					// minifier avec PHP
					foreach( $batch as $file )
					{
						$f = Tools::ensureTrailingSlash(WebAdminConfig::$ROOT) . $file;

						// tenter de minifier MatthiasMullie\Minify ; si erreur, recopier tel quel
						if ( $st = Tools::minifyWithPHP($f, str_replace('.js', ".min.js", $f)) )
						{
							$fjs = fopen(str_replace('.js', ".min.js", $f), 'w');
							fwrite($fjs, file_get_contents($f));
							fclose($fjs);
							
							$fundata['feedback'] .= $file . ' - non optimisé (Erreur Google Closure API : ' . $err['code'] . '  -  ' . $err['error'] . ' ET PHP::minify : ' . $st .")\n";
						}
						else
							// si pas d'erreur (ST = FALSE)
							$fundata['feedback'] .= $file . ' - optimisé avec MatthiasMullie\\Minify (Erreur Google Closure API : ' . $err['code'] . '  -  ' . $err['error'] . ")\n";
						
						$fundata['count']++;
					}
						
					continue;
				}
				
				
				// traiter maintenant l'optimisation, google renvoyant chaque fichier séparé par // input 0, puis // input 1, etc.
				for ( $i = 0 ; $i < count($batch) ; $i++ )
				{
					// l'expression régulière est différente pour le dernier batch, où il n'y a pas de suivant
					$ereg = ($i < count($batch) - 1) ? "~// Input $i\n(.*)// Input~Us" : "~// Input $i\n(.*)~s";
					
					// extraire le fichier optimisé
					$regs = [];
					if ( preg_match($ereg, $json['compiledCode'], $regs) && (count($regs) == 2) ) // U pour Ungreedy, s pour que .* capture des sauts de ligne
					{
						$fi = fopen(str_replace('.js', ".min.js", Tools::ensureTrailingSlash(WebAdminConfig::$ROOT) . $batch[$i]), 'w');
						fwrite($fi, $regs[1]);
						fclose($fi);
						
						$fundata['count']++;
						$fundata['feedback'] .= $batch[$i] . "\n";
					}
					
					else
					{
						curl_close($c);
						return "Erreur dans traitement batch, optimisation de '" . $batch[$i] . "' introuvable.";
					}
				}
			}
			
			
			// temporiser puis passer au batch suivant
			sleep(1);
		}
		
		
		curl_close($c);
		
		return NULL;
	}
	

	// supprimer un fichier optimisé
	static function _del_opt($f, &$fundata, Profile $appcfg)
	{
		if ( file_exists($f) )
		{
			unlink($f);
			$fundata['count']++;
			$fundata['feedback'] .= str_replace(Tools::ensureTrailingSlash(WebAdminConfig::$ROOT), '', $f) . "\n";
			return NULL;
		}
		else
			return "Erreur à la suppression du fichier optimisé '$f' : fichier introuvable.";
	}
	
	
	// activer/désactiver optimisation site
	static function _optOnOff($on, Profile $appcfg)
	{
		// lecture du manifest
		$m = Tools::readManifest($appcfg);
		if ( !$m )
			return "Manifest illisible '" . $appcfg->APP_MANIFEST . "'.";
			
		$txt = preg_replace('/Manifest::MINIFY[ ]*=>[ ]*(true|false)/', 'Manifest::MINIFY => ' . ($on ? 'true':'false'), $m['manifest']);

		$fi = fopen($m['filename'], 'w');
		fwrite($fi, $txt);
		fclose($fi);
		
		return NULL;
	}
	
	
	// nouveau versionning
	static function _optVersionning(Profile $appcfg, $ver, &$data)
	{
		if ( count($appcfg->OPT_VERSIONNING__FILES) )
		{
			// faire la liste des fichiers à versionner
			$root = Tools::ensureTrailingSlash(WebAdminConfig::$ROOT . $appcfg->OPT__ROOT);
			$script = str_replace('//', '/', "ls $root" . implode(" $root", $appcfg->OPT_VERSIONNING__FILES));

			if ( $files = shell_exec($script) )
			{
				// traiter tous les fichiers
				$files = explode("\n", $files);
				foreach ( $files as $f )
					if ( $f = trim($f) ) 	// effacer fin de ligne 0D
					{
						$fcontent = preg_replace($appcfg->OPT_VERSIONNING__PATTERN, str_replace('%', $ver, $appcfg->OPT_VERSIONNING__REPLACE), file_get_contents($f));
	
						$fh = fopen($f, 'w');
						fwrite($fh, $fcontent);
						fclose($fh);
						
						$data['count']++;
						$data['feedback'] .= str_replace(Tools::ensureTrailingSlash(webAdminConfig::$ROOT), '', $f) . "\n";
					}
			}
			else
				return "Erreur de versionning ; énumération en erreur.";
		}


		return NULL;
	}
	
	
}

?>