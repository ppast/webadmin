<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands\Helpers;



// [clauses use
use \Ppast\Webadmin\Config\Domain;
use \Ppast\Webadmin\Config\WebAdminConfig;
use \Ppast\Webadmin\Includes\Tools;
// clauses use]



// classe
class Websec 
{
	
	// supprimer une IP proscrite
	static function _websec_hta_ip_delete (Domain $domcfg, $ip)
	{
		// mode effacement
		return self::_websec_ip_update($domcfg, $ip, true);
	}


	// maj ip bloquées dans htaccess
	static function _websec_ip_update(Domain $domcfg, $ip, $delete = false)
	{
		// racine
		$root = Tools::ensureTrailingSlash(WebAdminConfig::$ROOT);
		
		// lire htaccess actuel
		$hta = file_get_contents(Tools::ensureTrailingSlash(WebAdminConfig::$ROOT) . '.htaccess');
		
		// faire une copie de sauvegarde du .htaccess
		$f = fopen($root . '.htaccess.old', 'w');
		fwrite($f, $hta);
		fclose($f);
		
		// extraire les ip
		$ips = self::_websec_analyse_ips($domcfg, $hta);
		if ( $ips === FALSE )
			return 'Impossible d\'identifier les IPs proscrites.';
		
		// si suppression de l'IP	
		if ( $delete )
		{
			$k = array_search($ip, $ips);
			if ( $k === FALSE )
				return "Impossible de trouver l'IP '$ip' dans la liste des IPs proscrites.";
			
			// effacer la valeur
			unset($ips[$k]);
		}
		else
		{
			// traiter toutes les IPs en arguements
			$ipupdate = explode(',', $ip);
			$dirty = false;
			foreach ( $ipupdate as $ip )
				// si existe déjà, passer à itération suivante
				if ( in_array($ip, $ips) )
					continue;
				else 
				{
					// sinon, indiquer qu'on a au moins une IP à rajouter
					$dirty = true;
					$ips[] = $ip;
				}
				
			// si ajout : trier, sinon quitter
			if ( $dirty )
				sort($ips, SORT_NATURAL | SORT_FLAG_CASE);
			else
				return NULL;
		}
		
		
		// transformer le tableau
		$newip_mask = WebAdminConfig::$ROOT_CFG->HTA__NEW_IP;
		$ips = array_map(function($v) use ($newip_mask) {
						return str_replace('%', $v, $newip_mask);
					}, 
					$ips);
		
		// modifier le htaccess
		$hta = preg_replace('{' . WebAdminConfig::$ROOT_CFG->HTA__BEGIN_TAG_IP_DENY . '(.*)' . WebAdminConfig::$ROOT_CFG->HTA__END_TAG_IP_DENY . '}s', 
							WebAdminConfig::$ROOT_CFG->HTA__BEGIN_TAG_IP_DENY . "\r\n" . implode("\r\n", $ips) . "\r\n" . WebAdminConfig::$ROOT_CFG->HTA__END_TAG_IP_DENY, $hta);
		
		// maj du fichier
		return self::_websec_htaccess_update($domcfg, $hta);
	}
	
	
	// mise à jour du htaccess
	static function _websec_htaccess_update(Domain $domcfg, $hta)
	{
		// vérifier conformité avant d'écrire
		if ( strpos($hta, WebAdminConfig::$ROOT_CFG->HTA__TEST_HTACCESS) === FALSE )
			return 'Modification du .htaccess en erreur. Expression de test non trouvée.';

		// racine
		$root = Tools::ensureTrailingSlash(WebAdminConfig::$ROOT);
		
		// modifier droits et écrire le fichier
		// si fichier protégé, le déprotéger temporairement
		$dochmod = false;
		if ( substr(decoct(fileperms($root . '.htaccess')),-4) == '0404' )
		{
			$dochmod = true;
			chmod($root . '.htaccess', 0604);
		}
		
		$f = fopen($root . '.htaccess', 'w');
		fwrite($f, $hta);
		fclose($f);

		if ( $dochmod )
			chmod($root . '.htaccess', 0404);
		
		return NULL;
	}
	

	// maj user agents dans htaccess
	static function _websec_ua_update(Domain $domcfg, $ua, $delete = false)
	{
		// racine
		$root = Tools::ensureTrailingSlash(WebAdminConfig::$ROOT);
		
		// lire htaccess actuel
		$hta = file_get_contents(Tools::ensureTrailingSlash(WebAdminConfig::$ROOT) . '.htaccess');
		
		// faire une copie de sauvegarde du .htaccess
		$f = fopen($root . '.htaccess.old', 'w');
		fwrite($f, $hta);
		fclose($f);
		
		// extraire les ua
		$uas = self::_websec_ua_useragents($domcfg, $hta);
		if ( $uas === FALSE )
			return 'Impossible d\'identifier les user-agents.';
		
		
		// si suppression
		if ( $delete )
		{
			$k = array_search($ua, $uas);
			if ( $k === FALSE )
				return "Impossible de trouver le user-agent '$ua' dans la liste des user-agents proscrits.";
			
			// effacer la valeur
			unset($uas[$k]);
		}
		
		// si ajout
		else
		{
			// ajouter cet ua (après avoir vérifié pas de doublon), et trier le tableau
			if ( in_array($ua, $uas) )
				return NULL;
				
			$uas[] = $ua;
			sort($uas, SORT_STRING | SORT_FLAG_CASE);
		}
		
		// transformer le tableau
		$newua_mask = WebAdminConfig::$ROOT_CFG->HTA__NEW_UA;
		$uas = array_map(function($v) use ($newua_mask) {
						return str_replace('%', $v, $newua_mask);
					}, 
					$uas);
		
		// modifier le htaccess
		$hta = preg_replace('{' . WebAdminConfig::$ROOT_CFG->HTA__BEGIN_TAG_UA_DENY . '(.*)' . WebAdminConfig::$ROOT_CFG->HTA__END_TAG_UA_DENY . '}s', 
							WebAdminConfig::$ROOT_CFG->HTA__BEGIN_TAG_UA_DENY . "\r\n" . implode("\r\n", $uas) . "\r\n" . WebAdminConfig::$ROOT_CFG->HTA__END_TAG_UA_DENY, $hta);
		
		// maj du fichier
		return self::_websec_htaccess_update($domcfg, $hta);
	}
		
	
	// extraire la liste des user-agents	
	static function _websec_ua_useragents(Domain $domcfg, $hta)
	{
	    $regs = [];
		// extraire zone des user-agents bloqués (flag S pour que "." match aussi les sauts de ligne)
		if ( preg_match('{' . WebAdminConfig::$ROOT_CFG->HTA__BEGIN_TAG_UA_DENY . '(.*)' . WebAdminConfig::$ROOT_CFG->HTA__END_TAG_UA_DENY . '}s', $hta, $regs) )
			// maintenant identifier toutes les ua
			if ( preg_match_all(WebAdminConfig::$ROOT_CFG->HTA__UA, $regs[0], $regs) === FALSE )
				return false;
			else
			{
				// regs est un tableau à 2 dimensions. $regs[0] = toutes les valeurs possibles pour le match du pattern complet, $regs[1] toutes les valeurs pour la première parenthèse
				$hta = $regs[1];
				return $hta;
			}
		else
			return false;
	}
	
	
	// analyser log domaine user agents
	static function _websec_ua(Domain $domcfg, $dt, &$data)
	{
		// obtenir log
		$output = self::_websec_get_log($domcfg, $dt);
		
		// si erreur, on a un tableau (statut=false, message)
		if ( is_array($output) && !$output['status'] )
			return $output['message'];
		
// 77.73.2.192 www.assistance-multimedia63.fr - [26/May/2014:05:30:54 +0200] "GET //wp-login.php HTTP/1.1" 403 221 "-" "Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm"
			
		
		// décoder le fichier GZ (si nécessaire ; les logs du jour OSL ne sont pas compressées)
		if ( date("Ymd", $dt) != date("Ymd", time()) )
			$output = gzdecode($output);
			
			
			
		// lire le fichier HTACCESS pour faire un cache des IP déjà proscrites et UA
		$hta = file_get_contents(Tools::ensureTrailingSlash(WebAdminConfig::$ROOT) . '.htaccess');

		// obtenir les IPs proscrites
		$ips = self::_websec_analyse_ips($domcfg, $hta);
		if ( $ips === FALSE )
			return "Impossible d'identifier les IPs proscrites depuis le .htaccess.";

		// obtenir les user agents
		$hta = self::_websec_ua_useragents($domcfg, $hta);
		if ( $hta === FALSE )
			return "Impossible d'identifier les user-agents proscrits depuis le .htaccess.";

		// constituer expression régulière avec les ua	
		$hta = '{' . implode('|', $hta) . '}i';



		// lire ligne par ligne
		$lines = explode("\n", $output);
		unset($output);
		

		// faire liste IP exclues de l'analyse
		$ipexcl = array_merge(WebAdminConfig::$ROOT_CFG->LOG__EXCLUDE_IP, WebAdminConfig::$ROOT_USER_CFG->LOG__EXCLUDE_IP);
				
		
		// tableau des lignes à analyser
		$ret = array();
		$uas = array();
		foreach ( $lines as $l )
		{
			// regarder si IP est exclue
			$p = strpos($l, ' ');
			if ( !($p === FALSE) )
				if ( !in_array(substr($l, 0, $p), $ipexcl) )
				{
					// découper ligne
					$line = explode(' ', $l);
					
					// le user-agent est dans la case 11 et suivantes
					$ua = implode(' ', array_slice($line, 11));
					
					// si pas connu, l'ajouter
					if ( !in_array($ua, $uas) )
					{
						$uas[] = $ua;
						
						// ne garder que début de la ligne, jusqu'aux cellules représentant le user-agent, et ajouter 1 seule case pour le user-agent fusionné
						$line = array_slice($line, 0, 11);
						
						// faire passer les colonnes 5 et 6 (HTTP METHOD + URL) à la fin du tableau, pour pouvoir lire sans utiliser l'ascenseur
						$line = array_merge(array_slice($line, 0, 5), array(str_replace('"', '', $ua)), array_slice($line, 5));

						//unset($line[11]); // supprimer referrer pour pouvoir voir le ua à l'écran sans utiliser l'ascenseur

						$ret[] = array(
								'newua' => !preg_match($hta, $line[5]),
								'newip' => !in_array($line[0], $ips),
								'line'  => $line
							);						
						

						$data['count']++;
					}
				}
		}
		
		
		if ( count($ret) )
		{
			// renvoyer tableau formaté
			$data['body'] = $ret;
		}
		
		
		// extraire les logs du jour
		return NULL;
	}



	// extraire la liste des ips proscrites
	static function _websec_analyse_ips(Domain $domcfg, $hta)
	{
		// extraire zone des user-agents bloqués (flag S pour que "." match aussi les sauts de ligne)
		$regs = [];
		if ( preg_match('{' . WebAdminConfig::$ROOT_CFG->HTA__BEGIN_TAG_IP_DENY . '(.*)' . WebAdminConfig::$ROOT_CFG->HTA__END_TAG_IP_DENY . '}s', $hta, $regs) )
			// maintenant identifier toutes les adresses IP
			if ( preg_match_all('/[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/', $regs[0], $regs) === FALSE )
				return false;
			else
			{
				// regs est un tableau à 2 dimensions. $regs[0] = toutes les valeurs possibles pour le match du pattern complet, $regs[1] toutes les valeurs pour la première parenthèse
				$hta = $regs[0];
				return $hta;
			}
		else
			return false;
	}


	// obtenir log
	static function _websec_get_log(Domain $domcfg, $dt, $retry = 0)
	{
		$c = curl_init();
		
		// préparer URL : remplacer % par le domaine, et [...] par le formatage de date correspondant
		$url = (date("Ymd", $dt) == date("Ymd", time())) ? $domcfg->LOG__OSL_URL : $domcfg->LOG__URL;
		$url = preg_replace_callback('/{(.*)}/U', function($regs) use ($dt) { return date($regs[1], $dt); }, $url);

		// définir URL
		curl_setopt($c, CURLOPT_URL, $url);

		// on fait une requête GET
		curl_setopt($c, CURLOPT_POST, false);
		
		// on veut récupérer les données renvoyées par le serveur
		curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
		
		// transmettre user et pwd
		curl_setopt($c, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($c, CURLOPT_USERPWD, $domcfg->LOG__USER . ":" . $domcfg->LOG__PASSWD);
		
		// timeout
		curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 3);
		curl_setopt($c, CURLOPT_TIMEOUT, 10);
		
		// exécuter la requête
		$output = curl_exec ($c);
		
		// obtenir un code d'erreur HTTP
		$i = curl_getinfo($c,  CURLINFO_HTTP_CODE);
		
		// HTTP 200 : ok
		if ( $i != 200 ) 
		{
			// si erreur ssl, retenter plusieurs fois
			if ( is_int(strpos(curl_error($c), 'Unknown SSL protocol error')) )
				// si pas encore re-tenté 5 fois, appel récursif
				if ( $retry < 5 )
				{
					usleep(500);
					return self::_websec_get_log($domcfg, $dt, $retry+1);
				}

            // si erreur 5xx (502 : bad gateway)
            else if ( substr("$i", 0, 1) == '5' )
                // si pas encore re-tenté 5 fois, appel récursif
                if ( $retry < 5 )
                {
                    usleep(500);
                    return self::_websec_get_log($domcfg, $dt, $retry+1);
                }
				
				
			// si autre erreur, ou si tenté 5 fois sans succès, échouer			
			$ret = "Erreur HTTP $i au traitement des statistiques OVH : " . curl_error($c);
			curl_close($c);
			return array('status'=>false, 'message'=>$ret);
		}
		else
		{
			curl_close($c);
			return $output;
		}
	}
	

	// analyser log domaine
	static function _websec_analyse(Domain $domcfg, $dt, &$data, $json_output = false)
	{
		$data = array('count'=>0, 'body'=>'');


		// obtenir log
		$output = self::_websec_get_log($domcfg, $dt);
		
		// si erreur, on a un tableau (statut=false, message)
		if ( is_array($output) && !$output['status'] )
			return $output['message'];

// 77.73.2.192 www.assistance-multimedia63.fr - [26/May/2014:05:30:54 +0200] "GET //wp-login.php HTTP/1.1" 403 221 "-" "Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm"
		
		// décoder le fichier GZ (si nécessaire ; les logs du jour OSL ne sont pas compressées)
		if ( date("Ymd", $dt) != date("Ymd", time()) )
			$output = gzdecode($output);
			
			
			
		// lire le fichier HTACCESS pour faire un cache des IP déjà proscrites
		$hta = file_get_contents(Tools::ensureTrailingSlash(WebAdminConfig::$ROOT) . '.htaccess');
		$hta = self::_websec_analyse_ips($domcfg, $hta);
		if ( $hta === FALSE )
			return 'Impossible d\'identifier les IPs proscrites.';


		// préparer préfixes à rechercher (fusion préfixes domaine + préfixes communs)
		$prefixes = array_merge($domcfg->LOG__PREFIXES_OK, WebAdminConfig::$ROOT_CFG->LOG__PREFIXES_OK);
		$prefixes = implode('|', $prefixes);

		// lire ligne par ligne
		$lines = explode("\n", $output);
		unset($output);
		
		
		// faire liste IP exclues de l'analyse
		$ipexcl = array_merge(WebAdminConfig::$ROOT_CFG->LOG__EXCLUDE_IP, WebAdminConfig::$ROOT_USER_CFG->LOG__EXCLUDE_IP);
		
		
		// tableau des lignes à analyser
		$ret = array();
		foreach ( $lines as $l )
		{
			// regarder si IP est exclue
			$p = strpos($l, ' ');
			if ( !($p === FALSE) )
				if ( !in_array(substr($l, 0, $p), $ipexcl) )
				{
					// découper ligne
					$line = explode(' ', $l, 12);
					
					// regarder si l'uri commence par un des préfixes configurés ; si non, prendre la ligne en compte
					$regs = [];
					if ( !preg_match('`' . $prefixes . '`', $line[6], $regs) )
					{
						$data['count']++;


						// traiter différement selon output
						$ret[] = array(
											'line'		=> $line,
											'newip'		=> !in_array($line[0], $hta)
										);
					}
				}
		}
		
		

		$data['body'] = $ret;

		
		// pas d'erreur
		return NULL;
	}

	
}

?>