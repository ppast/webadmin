<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Domain;
use \Ppast\Webadmin\Config\WebAdminConfig;
use \Ppast\Webadmin\Includes\Tools;
// clauses use]



class Composer_devdelete extends \Ppast\Webadmin\Commands\Base
{
	public function run(Domain $domcfg)
	{
		// chemin de dev
		$devc = Tools::ensureTrailingSlash(Tools::ensureTrailingSlash(WebAdminConfig::$ROOT) . $domcfg->COMPOSER_DEV_PATH);

		// package vendor/name
		$package = $_REQUEST['package'];
		
		// exécuter supprimer
		$ret = shell_exec("rm -f -r {$devc}$package");
		
		if ( $ret && trim($ret) )
			return $this->status(false, 'Impossible de supprimer le package en développement.', Tools::consoleOutput($ret), true);
		
		return $this->status(true, 'Package en développement supprimé avec succès.');
	}
	
}


?>