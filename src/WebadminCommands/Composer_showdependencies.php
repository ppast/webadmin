<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Domain;
use \Ppast\Webadmin\Includes\Tools;
// clauses use]



class Composer_showdependencies extends \Ppast\Webadmin\Commands\Base
{
	public function run(Domain $domcfg)
	{
        try
        {
            $ret = \Ppast\Webadmin\WebadminCommands\Helpers\ComposerInterface::get()->package_show_dependencies($_REQUEST['package']);
            return $this->status(true, 'Show dependencies exécuté', Tools::consoleOutput($ret), true);
        }
        catch(\Throwable $e)
        {
            return $this->status(false, 'Une exception est survenue : ', Tools::consoleOutput($e->getMessage()), true);
        }
	}
	
}


?>