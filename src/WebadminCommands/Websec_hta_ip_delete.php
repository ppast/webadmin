<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Domain;
// clauses use]



class Websec_hta_ip_delete extends \Ppast\Webadmin\Commands\Base
{
	public function run(Domain $domcfg)
	{
		if ( $st = Helpers\Websec::_websec_hta_ip_delete($domcfg, $_REQUEST['ip']) )
			return $this->status(false, $st);
		else	
			return $this->status(true, "Suppression de l'IP proscrite '" . $_REQUEST['ip'] . "' effectuée.");
	}
	
}


?>