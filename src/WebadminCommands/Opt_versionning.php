<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Profile;
// clauses use]



class Opt_versionning extends \Ppast\Webadmin\Commands\Base
{
	public function run(Profile $appcfg)
	{
		// définir version
		$ver = "v" . date('YmdHis');
		
		$data = array('count' => 0, 'feedback' => '');
		if ( $st = Helpers\Opt::_optVersionning($appcfg, $ver, $data) )
			return $this->status(false, $st);
		
		return $this->status(true, "Traitement effectué : " . $data['count'] . " fichier(s) versionné(s) en '$ver'.", $data['feedback']);
	}
	
}


?>