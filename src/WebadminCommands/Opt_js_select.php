<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Profile;
use \Ppast\Webadmin\Config\WebAdminConfig;
use \Ppast\Webadmin\Includes\Tools;
use \Ppast\Webadmin\Config\Core;
// clauses use]



use \Ppast\Webadmin\WebadminWebInterface\Controller;



class Opt_js_select extends \Ppast\Webadmin\Commands\Base
{
	// affichage
	protected function _renderCSS_JS()
	{
		parent::_renderCSS_JS();


		// config depuis contexte controller
		$appcfg = WebAdminConfig::$APPS[Controller::$CONTEXT->value];


		?><script>
	
			window.webadmin = window['webadmin'] || {};
			webadmin.opt_js = webadmin['opt_js'] || {
				submit : function(form)
				{
					// préparer requête avec énumération des ressources
					var ret = new Array();
					
					// obtenir les checkbox
					var elementsl = form.elements.length;
					for ( var i = 0 ; i < elementsl ; i++ )
					{
						var e = form.elements[i];
						if ( e.type == 'checkbox' )
							if ( e.checked )
								ret.push(e.value);
					}
					
					// si une sélection faite
					if ( ret.length )
					{
						nettools.jscore.SecureRequestHelper.post(
								// URL
								'<?php echo \Ppast\Webadmin\WebInterface\CoreController::$routage; ?>',
								
								// data
								{
									u : '<?php echo \Ppast\Webadmin\WebInterface\CoreController::$USER; ?>',
									app : '<?php echo $appcfg->name; ?>',
									cmd : 'opt_js_selected',
									files : ret.join(';')
								}
							);
					}
				}
			};
			
			</script><style>
			.opt_js input[type='checkbox'] {
				padding:0px;
				margin:0px;
				border:0px;
				font-family:inherit;
				font-weight:bold;
				background-color:transparent;
			}
	
			</style><?php
	}
	

	// affichage
	protected function _renderBody()
	{
		echo '<form name="foptjs" method="post" action=""><table cellpadding="0" cellspacing="0" border="0" class="webadminList opt_js">';
		foreach ( $this->_body as $line )
		{
			$css = !empty($line['updated']) ? 'highlight2' : '';
			echo '<tr><td>' . $line['file'] . "</td><td class=\"highlight $css\">" . date('d/m/Y H:i:s', $line['mtime']) . '</td><td><input type="checkbox" value="' . $line['file'] ."\"></td></tr>\n";
		}
		
		// rajouter un bouton submit
		echo "<tr><td><input type=\"button\" value=\"Optimiser\" onclick=\"webadmin.opt_js.submit(this.form); \"></td><td>&nbsp;</td></tr>";

		echo '</table></form>';
	}
	
	
	public function run(Profile $appcfg)
	{
		$data = array('count'=>0, 'body'=>'');
		

		// on fait d'abord liste des fichiers ; _minify_js ne modifie pas le fichier, mais produit juste dans $fundata['files'] une URL au lieu d'un chemin absolu
		$fundata = array('files'=>array());
		if ( $st = Helpers\Opt::_folder(WebAdminConfig::$ROOT . $appcfg->OPT__ROOT, '*.js', array('\Ppast\Webadmin\WebadminCommands\Helpers\Opt', '_minify_js'), $fundata, $appcfg) )
			return $this->status(false, $st);
		
			
		// constituer tableau			
		$ret = array();

		// pour toutes les ressources 
		$now = date('Ymd');
		foreach ( $fundata['files'] as $file )
		{
			$mtime = filemtime(Tools::ensureTrailingSlash(WebAdminConfig::$ROOT) . $file);
			$ret[] = array (
						'file' => $file,
						'updated' => date('Ymd', $mtime) === $now,
						'mtime' => $mtime
					);
		}
		

		if ( count($ret) )
		{
			// renvoyer tableau formaté
			$data['body'] = $ret;
			$data['count'] = count($ret); 
		}
		
		return $this->status(true, "Liste des ressources JS ci-dessous : " . $data['count'] . ' ressources disponibles.', $data['body'], true);
	}
	
}


?>
