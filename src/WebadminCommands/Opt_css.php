<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Profile;
use \Ppast\Webadmin\Config\WebAdminConfig;
// clauses use]



class Opt_css extends \Ppast\Webadmin\Commands\Base
{
	// optimiseur CSS
	public function run(Profile $appcfg)
	{
		$data = array('count' => 0, 'feedback' => '');
		if ( $st = Helpers\Opt::_folder(WebAdminConfig::$ROOT . $appcfg->OPT__ROOT, '*.css', array('\Ppast\Webadmin\WebadminCommands\Helpers\Opt', '_minify_css'), $data, $appcfg) )
			return $this->status(false, $st);
		else
		{
			return $this->status(true, "Traitement effectué : " . $data['count'] . ' fichier(s) optimisé(s).', $data['feedback']);
		}
	}
}


?>