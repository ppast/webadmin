<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Domain;
use \Ppast\Webadmin\Config\WebAdminConfig;
use \Ppast\Webadmin\Includes\Tools;
// clauses use]



class Composer_json extends \Ppast\Webadmin\Commands\Base
{
	public function run(Domain $domcfg)
	{
		$libc = Tools::ensureTrailingSlash(Tools::ensureTrailingSlash(WebAdminConfig::$ROOT) . $_REQUEST['libc']);
		$f = fopen($libc . 'composer.json', 'w');
		fwrite($f, $_REQUEST['composer_json']);
		fclose($f);


        try
        {
            $ret = \Ppast\Webadmin\WebadminCommands\Helpers\ComposerInterface::get()->validate();
            return $this->status(true, 'Composer.json modifié', Tools::consoleOutput($ret), true);
        }
        catch(\Throwable $e)
        {
            return $this->status(false, 'Une exception est survenue : ', Tools::consoleOutput($e->getMessage()), true);
        }

	}
	
}


?>