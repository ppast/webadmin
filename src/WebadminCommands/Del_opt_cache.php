<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Profile;
use \Ppast\Webadmin\Config\WebAdminConfig;
use \Ppast\Webadmin\Includes\Tools;
// clauses use]



class Del_opt_cache extends \Ppast\Webadmin\Commands\Base
{
	public function run(Profile $appcfg)
	{
		// lire le manifest
		$m = Tools::readManifest($appcfg);
		if ( !$m )
			return $this->status(false, "Lecture manifest impossible '" . $appcfg->APP_MANIFEST . "'.");
		
		// tenter d'extraire l'emplacement du cache (Manifest::MINIFY_CACHE => '/test/_cache/') ; (?: ==> parenthèse non capturante
		$regs = [];
		if ( !preg_match("~Manifest::APP_NAME[ ]*=>[ ]*(?:'|\")(.*)(?:'|\")~", $m['manifest'], $regs) )
			return $this->status(false, "Identification cache mode optimis&eacute; impossible.");
		
		$data = array('count' => 0, 'feedback' => '');
        if ( $st = Helpers\Opt::_folder(Tools::ensureTrailingSlash(Tools::ensureTrailingSlash(webAdminConfig::$ROOT) . Tools::ensureTrailingSlash($appcfg->APP_ROOT) . $regs[1]), '_cache/*.cache', 
									array('\Ppast\Webadmin\WebadminCommands\Helpers\Opt', '_del_opt'), $data, $appcfg, true, false) )
			return $this->status(false, $st);
		else
			return $this->status(true, "Traitement effectué : " . $data['count'] . ' fichier(s) cache effacé(s).', $data['feedback']);
	}
	
}


?>