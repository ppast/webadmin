<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Domain;
use \Ppast\Webadmin\Includes\Tools;
// clauses use]



class Composer_show1 extends \Ppast\Webadmin\Commands\Base
{
	public function run(Domain $domcfg)
	{
        try
        {
            $ret = \Ppast\Webadmin\WebadminCommands\Helpers\ComposerInterface::get()->package_show($_REQUEST['package']);
            return $this->status(true, 'Show unitaire exécuté', Tools::consoleOutput($ret), true);
        }
        catch(\Throwable $e)
        {
            return $this->status(false, 'Une exception est survenue : ', Tools::consoleOutput($e->getMessage()), true);
        }
	}
	
}


?>