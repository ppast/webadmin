<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Domain;
use \Ppast\Webadmin\Config\WebAdminConfig;
// clauses use]



class Composer_updatetool extends \Ppast\Webadmin\Commands\Base
{
	public function run(Domain $domcfg)
	{
		// lire outil et effectuer les paramétrages
		$cmd = WebAdminConfig::$ROOT_CFG->COMPOSER_UPDATETOOL;
		$cmd = str_replace('§zip§', '"' . $_REQUEST['zip'] . '"', str_replace('§devdir§', $_REQUEST['sourcetree'], $cmd));

		// en-tête pour forcer le téléchargement et suggérer un nom
		header("Content-Type: application/octet-stream; name=\"sourcetree-update.cmd\"");
		header("Content-Disposition: attachment; filename=\"sourcetree-update.cmd\"");
		header("Expires: 0");
		header("Cache-Control: no-cache, must-revalidate");
		header("Pragma: no-cache"); 

		echo $cmd;
		die();
	}
	
}


?>