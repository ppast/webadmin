<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use

// clauses use]



class Users_update_console extends \Ppast\Webadmin\Commands\Base
{
	public function run()
	{
		$data = array("index"=>(integer)($_REQUEST['i']));

        
        // énumérer propriétés du user
        $user = array();

        foreach ( \Ppast\Webadmin\Config\Users::usersProvider()->userProperties() as $prop )
            $user[$prop] = $_REQUEST[$prop];
        
		// update user
		\Ppast\Webadmin\Config\Users::usersProvider()->updateUser($_REQUEST['_user'], (object) $user);


		return $this->status(true, NULL, $data);
	}
	
}


?>