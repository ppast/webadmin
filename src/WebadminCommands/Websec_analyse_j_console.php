<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\WebAdminConfig;
// clauses use]



class Websec_analyse_j_console extends \Ppast\Webadmin\Commands\Base
{
	public function run()
	{
		// répondre en fournissant, entre autres, le n°index donné en paramètre
		$data = array(
						'index'		=> (integer)($_REQUEST['i']),
						'domains'	=> NULL,
						'body'		=> array()
					);

		
		$j = $_REQUEST['j'];
		if ( $j == '0' )
			$dt = time();
		else
			$dt = strtotime("yesterday") - ((int)$j-1) * 24*60*60;
			
			
		// pour tous les domaines
		foreach ( WebAdminConfig::$DOMAINS as $domcfg )
		{
			// analyser domaine ; si erreur, une chaine est renvoyée
			$ret = array('count'=>0, 'body'=>'');
			if ( $st = Helpers\Websec::_websec_analyse($domcfg, $dt, $ret, true) )
				return $this->status(false, "Erreur analyse domaine '" . $domcfg->name . "' : $st", $data);
				
			// sinon, arrivé ici, on a dans $ret['body'] le json
			$data['body'][$domcfg->name] = $ret['body'];
		}
		

		return $this->status(true, NULL, $data);
	}
	
}


?>