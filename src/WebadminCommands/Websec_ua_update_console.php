<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\WebAdminConfig;
// clauses use]



class Websec_ua_update_console extends \Ppast\Webadmin\Commands\Base
{
	public function run()
	{
		// répondre en fournissant, entre autres, le n°index donné en paramètre
		$data = array("index"=>(integer)($_REQUEST['i']), 'domains'=>NULL);
		$ret = array();
		
		// pour tous les domaines qui acceptent une mise à jour télécommandée
		foreach ( WebAdminConfig::$DOMAINS as $domcfg )
			if ( $domcfg->HTA__CONSOLE_UPDATE )
			{
				// mise à jour et stocker le résultat : NULL si ok, chaine avec message d'erreur sinon
				$ret[$domcfg->name] = Helpers\Websec::_websec_ua_update($domcfg, $_REQUEST['ua']);
			}
			
		// renvoyer
		$data['domains'] = $ret;
		return $this->status(true, NULL, $data);
	}
	
}


?>