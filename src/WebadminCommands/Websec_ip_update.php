<?php

// namespace
namespace Ppast\Webadmin\WebadminCommands;


// [clauses use
use \Ppast\Webadmin\Config\Domain;
// clauses use]



class Websec_ip_update extends \Ppast\Webadmin\Commands\Base
{
	public function run(Domain $domcfg)
	{
		if ( $st = Helpers\Websec::_websec_ip_update($domcfg, $_REQUEST['ip']) )
			return $this->status(false, $st);
		else	
			return $this->status(true, "Mise à jour des IPs proscrites effectuée.");
	}
	
}


?>