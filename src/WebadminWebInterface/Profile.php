<?php

// namespace
namespace Ppast\Webadmin\WebadminWebInterface;



// clauses use
use \Ppast\Webadmin\Config\WebAdminConfig;
use \Ppast\Webadmin\Config\Plugin;
use \Ppast\Webadmin\Config\Core;
use \Ppast\Webadmin\Includes\Tools;
use \Ppast\Webadmin\Auth\AuthHandler;
use \Ppast\Webadmin\WebInterface\CoreController;




class Profile 
{
	// affichage
	static function render($u, Context $appctx, \Ppast\Webadmin\Config\Base $cfg = NULL)
	{
		$app = $appctx->isProfile();
		?>
		<li><b><span class="profileDomainWebadmin <?php if ( $app ) echo 'profileDomainWebadminActive'; ?>">Choisir profil</span> (<?php echo $app; ?>) : </b><?php 

		// afficher choix applications
		foreach ( WebAdminConfig::$APPS as $k=>$a )
		{
			if ( \Ppast\Webadmin\Config\Users::usersProvider()->isUserAuthorized($u, $a->PROFILE_GRANT) !== FALSE )
			{
				// tester sécurité de ce profil
				if ( !Tools::testSec($a) )
					$secured = " class=\"profileSecOff\"";
				else
					$secured = "";
				
				echo "<a href=\"" . CoreController::$routage . "?app=$k&u=$u\" $secured data-csrf=\"1\">$k</a>, ";
			}
		}
		
		?></li>
		<?php
		
		// si application choisie
		if ( $app )
		{
			// si site sécurisé par chmod
			if ( $cfg->PROFILE_SEC )
			{
				?>
				<li style="margin-top:15px;">Verrouillage <?php echo $cfg->SEC__METHOD; ?> (<b><?php echo (Tools::testSec($cfg)?'activé':'<span class="profileSecOff">désactivé</span>'); ?></b>) : 
					<a href="<?php echo CoreController::$routage; ?>?cmd=sec_on&app=<?php echo $app; ?>&u=<?php echo $u; ?>" data-csrf="1" data-waitcursor="1">Activer</a>
					- <a href="<?php echo CoreController::$routage; ?>?cmd=sec_off&app=<?php echo $app; ?>&u=<?php echo $u; ?>" data-csrf="1" data-waitcursor="1">Désactiver</a> | <i><?php echo self::foldersSec($cfg); ?></i></li>
				<?php
			}


			// si app sécurisée, certaines actions ne sont pas dispo
			if ( Tools::testSec($cfg) )
			{
				?>
				</ul>
				<div class="webAdminSecOn" id="webAdminSecOn">
				<ul>
				<?php
			}
				
				
			// les options autres que la sécurisation ne sont accessibles que pour superuser
			if ( \Ppast\Webadmin\Config\Users::usersProvider()->isUserAuthorized($u, array(\Ppast\Webadmin\Config\Users::$SUPERUSER)) !== FALSE )
			{
				// si optimisation activée
				if ( $cfg->PROFILE_OPT )
				{
					// si framework appli
					if ( $cfg->APP_FRMK_FRAMEWORK )
					{
						?>
						<li style="margin-top:15px;">Mode optimisé (<b><?php echo (Tools::testOpt($cfg)?'activé':'<span class="profileSecOff">désactivé</span>'); ?></b>) :
							<a href="<?php echo CoreController::$routage; ?>?cmd=opt_on&app=<?php echo $app; ?>&u=<?php echo $u; ?>" data-csrf="1">Activer</a>
							- <a href="<?php echo CoreController::$routage; ?>?cmd=opt_off&app=<?php echo $app; ?>&u=<?php echo $u; ?>" data-csrf="1">Désactiver</a></li>
						<li><a href="<?php echo CoreController::$routage; ?>?cmd=del_opt_cache&app=<?php echo $app; ?>&u=<?php echo $u; ?>" data-csrf="1">Supprimer le cache du mode optimis&eacute;</a></li>
						<?php
					}
					?>
					<li style="margin-top:15px;">Optimiser : <a href="<?php echo CoreController::$routage; ?>?cmd=opt_css&app=<?php echo $app; ?>&u=<?php echo $u; ?>" data-csrf="1" data-waitcursor="1">sources CSS</a>
						- <a href="<?php echo CoreController::$routage; ?>?cmd=opt_js&app=<?php echo $app; ?>&u=<?php echo $u; ?>" data-csrf="1" data-waitcursor="1">sources JS</a>
						- <a href="<?php echo CoreController::$routage; ?>?cmd=opt_js_select&app=<?php echo $app; ?>&u=<?php echo $u; ?>" data-csrf="1" data-waitcursor="1">certaines sources JS</a>
						| <i><?php echo self::foldersOpt($cfg); ?></i>
					</li>
					<li>Supprimer les optimisations : <a href="<?php echo CoreController::$routage; ?>?cmd=del_opt_css&app=<?php echo $app; ?>&u=<?php echo $u; ?>" data-csrf="1" data-waitcursor="1">sources CSS</a> 
						- <a href="<?php echo CoreController::$routage; ?>?cmd=del_opt_js&app=<?php echo $app; ?>&u=<?php echo $u; ?>" data-csrf="1" data-waitcursor="1">sources JS</a>
						| <i><?php echo self::foldersOpt($cfg); ?></i> 
					</li>
					
					<?php
					// si versionning
					if ( $cfg->OPT_VERSIONNING )
					{
						?>
						<li style="margin-top:15px;">Versionning : <a href="<?php echo CoreController::$routage; ?>?cmd=opt_versionning&app=<?php echo $app; ?>&u=<?php echo $u; ?>" data-csrf="1">nouvelle version</a>
							- <a href="<?php echo CoreController::$routage; ?>?cmd=opt_versionningvXX&app=<?php echo $app; ?>&u=<?php echo $u; ?>" data-csrf="1">version vXX</a>
							| <i><?php echo self::filesVersionning($cfg); ?></i>
						</li>
						<?php
					}
				}
                
                
                // si site utilise app_framework
                if ( $cfg->APP_FRMK_FRAMEWORK )
                {
                    ?>
                    <form class="uiForm" name="fWebadmin" method="post" action="<?php echo CoreController::$routage; ?>">
                        <?php echo AuthHandler::getCSRFHelper()->addCSRFHiddenInput(); ?>
                        <input type="hidden" name="app" value="<?php echo $app; ?>">
                        <input type="hidden" name="u" value="<?php echo $u; ?>">
                        <input type="hidden" name="cmd" value="manifest_config">
                        <li style="margin-top:15px;">Paramètres de configuration : <a href="javascript:void(0)" onclick="var form = this.parentNode; while ( form && (form.nodeName != 'FORM') ) {form = form.parentNode; }; if ( !form ) return false; form.submit();">Enregistrer les modifications</a></li>
                        <textarea name="config" style="width:75%" rows="15" spellcheck="false"><?php echo Tools::readManifestConfig($cfg); ?></textarea>
                    </form>
                    <?php
                }
			}
				
				
			// si plugins
			if ( $cfg->PLUGINS )
			{
				// obtenir config des plugins
				Plugin::setupFromList($cfg->PLUGINS);
				
				// respecter l'ordre de $appcfg->PLUGINS ; en cas de commande faisant appel à un plugin, le premier élément de webAdminConfig::$PLUGINS
				// sera le plugin appelé, pas forcément le premier dans l'ordre demandé
				
				// pour tous les plugins de cette application
				foreach ( $cfg->PLUGINS as $kp )
				{
					$plugin = WebAdminConfig::$PLUGINS[$kp];
					
					// plugin autorisé selon niveau d'accès
					if ( $plugin && \Ppast\Webadmin\Config\Users::usersProvider()->isUserAuthorized($u, $plugin->ACCESS_LEVEL_REQUIRED) ) 
					{
						?>
						<li style="margin-top:15px;">Plugin '<?php echo $kp; ?>' : 
							<?php
							// chaque action est un tableau associatif (action=>libellé)
							foreach ( $plugin->ACTIONS as $kid => $action )
							{
								$title = ($plugin->DESCRIPTION && array_key_exists($kid, $plugin->DESCRIPTIONS)) ? $plugin->DESCRIPTIONS[$kid] : null;
								?>
								<a href="<?php echo CoreController::$routage; ?>?plugin=<?php echo $kp; ?>&cmd=<?php echo $kid; ?>&app=<?php echo $app; ?>&u=<?php echo $u; ?>" title="<?php echo $title; ?>" data-csrf="1"><?php echo $action; ?></a> -
								<?php
							}
							?>
						</li>
						<?php
					}
				}
			}
			
			
			// si app sécurisée, on ne peut pas faire certaines actions
			if ( Tools::testSec($cfg) )
			{
				?>
				</ul>
				</div>
				<ul>
				<script>
				disableAs('webAdminSecOn');
				</script>
				<?php
			}
			

			// url appli
			if ( $cfg->APP__URL )
			{
				?>
				<li style="margin-top:15px;"><a href="<?php echo $cfg->APP__URL; ?>" target="_blank">Ouvrir l'application</a></li>
				<?php
			}
		}
	}


	// lister dossiers concernés par sécurité
	private static function _foldersSec($script)
	{
		$f = file_get_contents(Core::$DATA_ROOT . "scripts_profiles/$script");
		//  for FOLDER in controle/ download/ images/ lib/ outils/ polar/ webadmin/ 
		
		// rechercher l'énumération des dossiers
		$regs = [];
		if ( preg_match("#for FOLDER in (.*)\n#", $f, $regs) )
			return trim($regs[1]);
		else
			return "";
	}
	
	
	// lister dossiers concernés par sécurité pour le profil
	private static function foldersSec($appcfg)
	{
		switch ( $appcfg->SEC__METHOD )
		{
			case 'intelligent':
				return implode (' ', array_map(function($item) use ($appcfg){return $appcfg->SEC__ROOT . $item . '/';},$appcfg->SEC__FOLDERS));
				
			case 'scripts':
				return self::_foldersSec($appcfg->SEC__SCRIPTS['on']);
		}
	}
	
	
	// obtenir liste dossiers à optimiser
	private static function foldersOpt($appcfg)
	{
		return implode(' ', array_map(function($item) use ($appcfg){return $appcfg->SEC__ROOT . $item . '/';}, $appcfg->OPT__INCLUDE_FOLDERS));
	}


	// obtenir liste fichiers concernés par le versionning
	private static function filesVersionning($appcfg)
	{
        // enlever // superflus suite à concaténation racine sec et item
		return implode(', ', array_map(function($item) use ($appcfg){return str_replace('//', '/', $appcfg->SEC__ROOT . $item);}, $appcfg->OPT_VERSIONNING__FILES));
	}

}

?>