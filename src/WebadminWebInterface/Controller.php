<?php

// namespace
namespace Ppast\Webadmin\WebadminWebInterface;



// clauses use
use \Ppast\Webadmin\Config\Core;
use \Ppast\Webadmin\Config\WebAdminConfig;
use \Ppast\Webadmin\Config\Webadmin;
use \Ppast\Webadmin\Config\Plugin;
use \Ppast\Webadmin\Auth\AuthHandler;
use \Ppast\Webadmin\WebInterface\CoreController;



// controller interface
class Controller extends CoreController
{
	// user et contexte requetes
	static $CONTEXT = NULL;
	
	
	// affichage
	static function process($data_root)
	{
		$st = NULL;
		$command = NULL;
		
		
		try
		{
			// appel parent
			CoreController::coreProcess($data_root, __FILE__);
	
			
			// contexte		
			self::$CONTEXT = Context::emptyContext();
	
			
			// faire l'inventaire des applications et domaines 
			WebAdminConfig::listApps();
			WebAdminConfig::listDomains();
	
			
	
	
			// ========================
			// TRAITER AUTHENTIFICATION
			// ========================
			
			// si arrivée par login URL, on a un USER, un PASSWORD, et en plus un TIMESTAMP pour éviter pénétration même si l'URL GET, visible, est interceptée
			if ( isset($_REQUEST['u']) && isset($_REQUEST['url_password']) && isset($_REQUEST['url_ts']) && (strtoupper($_SERVER['REQUEST_METHOD']) == 'GET') )
				self::$auth = AuthHandler::authenticateUrl($_REQUEST['u'], $_REQUEST['url_password'], $_REQUEST['url_ts']);
			// si soumission formulaire identification
			else if ( isset($_REQUEST['u']) && isset($_REQUEST['fLogin_password']) && isset($_REQUEST['fLogin_token']) && (strtoupper($_SERVER['REQUEST_METHOD']) == 'POST') )
				// authentifier l'utilisateur ; si pb, le script est arrêté dans authenticate() ; sinon, on récupère le token généré et mis en cookie
				self::$auth = AuthHandler::authenticateForm($_REQUEST['u'], hash('sha256',$_REQUEST['fLogin_password']), $_REQUEST['fLogin_token']);
			else if ( isset($_REQUEST['u']) && isset($_REQUEST['token']) )
				// sinon, si requete, authentifier selon jeton=sha256(cookie) en paramètre et cookie
				self::$auth = AuthHandler::authenticateRequest($_REQUEST);
			else
			{
				// sinon, arrivée sans token ; il peut s'agir de l'arrivée initiale sur la page de login, mais pas d'une mauvaise authentification (= FALSE)
				self::$auth = NULL;
				AuthHandler::halt();
			}
	
	
	
	
			// ========================
			// TRAITEMENT DES COMMANDES
			// ========================
			$cfg = NULL;
			
			
			// commandes uniquement traitées si identification OK
			if ( self::$auth )
			{
				// déterminer profil géré et inclure ses paramètres
				if ( isset($_REQUEST['app']) && ($app = $_REQUEST['app']) )
				{
					self::$CONTEXT = new Context('app', $app);
					$cfg = WebAdminConfig::$APPS[$app];
				}
				
				// déterminer domaine géré et inclure ses paramètres
				elseif ( isset($_REQUEST['dom']) && ($dom = $_REQUEST['dom']) )
				{
					self::$CONTEXT = new Context('dom', $dom);
					$cfg = WebAdminConfig::$DOMAINS[$dom];
				}
					
				// si gestion webadmin
				elseif( isset($_REQUEST['webadmin']) && ($webadmin = $_REQUEST['webadmin']) )
				{
					self::$CONTEXT = new Context('webadmin', $webadmin);
					$cfg = Webadmin::setupFromFile($webadmin); // les actions sur domaines et apps sont partagées, mais pas les webadmins (utilisateurs, réplication, etc.)
				}
				
				// cas où ni profil ni domaine sélectionné ni gestion webadmin
				else $cfg = NULL;
			
				
				
				// si plugin, c'est un cas particulier des commandes
				if ( isset($_REQUEST['plugin']) )
				{
					$cfgplugin = Plugin::setupFromFile($_REQUEST['plugin']);
					self::$CONTEXT->userdata['plugin'] = $cfgplugin->name;
			
					// paramètres : config appli, config plugin, commande dynamique à exécuter 
					$params = array($cfg, $cfgplugin, $_REQUEST['cmd']);
					$nscmd = '\\Ppast\\Webadmin\\WebadminCommands\\Plugins\\';
					$cmd = $_REQUEST['plugin'];
					
					// si plugin non standard, le rechercher dans /plugins du dossier user appelant
					if ( !in_array(ucfirst($cmd), WebAdminConfig::defaultPlugins()) )
						include(Core::$CALLER_ROOT . 'plugins/' . ucfirst($cmd) . '.php');
				}
				
				// cas général, commande simple
				else
				{
					$params = array($cfg);
					$nscmd = '\\Ppast\\Webadmin\\WebadminCommands\\';
					$cmd = isset($_REQUEST['cmd']) ? $_REQUEST['cmd']:'';
				}
				
				
				
				// exécution de la commande et récup booléen ok (le message, le body et les metadata statut sont dans la commande)
				$st = CoreController::coreCommandHandler($nscmd, $cmd, $params);
				$command = CoreController::$command;
			}
		}
					
		// intercepter toutes les erreurs, y compris les fatales, pour renvoyer un message clair si JSON-P
		catch(\Throwable $e)
		{
			// traiter différement selon json-p ou pas
			if ( isset($_REQUEST['jsonp']) && ($jsonp = $_REQUEST['jsonp']) )
			{
				$ret = array("status"=>false, 'message'=>$e->getMessage() . ' ; ' . $e->getFile() . ':' . $e->getLine());
				echo "$jsonp(" . json_encode($ret) . ');';
				die();
			}
			else
				throw $e;
		}




		// ========================
		// TRAITEMENT JSON-P
		// ========================
		
		// si appel en jsonp, la réponse va être traitée différemment, que ce soit pour la commande éventuelle, ou une erreur de login
		if ( isset($_REQUEST['jsonp']) && ($jsonp = $_REQUEST['jsonp']) )
		{
			// si authentification ko
			if ( self::$auth === FALSE )
				$ret = array("status"=>false, "message"=>'Erreur d\'authentification');
			
			// si arrivée sans token, on devrait afficher l'écran de login ; en JSON-P, on ne peut rien faire 
			else if ( self::$auth === NULL )
				$ret = array("status"=>false, 'message'=>'En JSON-P, un token est nécessaire pour toute commande');
			
			// si auth ok
			else
			{
				// si commande présente (en principe, en JSON-P, c'est le cas), $ST est défini et _BODY contient le body éventuel
				if ( $cmd )
				{
					// former une réponse pour json
					$ret = array(
								'status'		=> $st, 
								'message'		=> $command->getMessage(),
								'autofeedback'	=> $command->getStatusMetadata('autofeedback'),
								'response'		=> $command->getBody()
							);
				}
				
				// si commande absente, créer une réponse vide
				else
					$ret = array("status"=>true);
					
		
				// toujours inclure le token en réponse
				$ret['token'] = AuthHandler::getCSRFHelper()->getCSRFCookie();
			}
		
            // envoyer header application/javascript
            header('Content-Type: application/javascript; charset=utf-8');
			echo "$jsonp(" . json_encode($ret) . ');';
		
			// si pas de commande, cas en principe impossible, renvoyer page blanche pour ne pas avoir d'erreur JS
			die();
		}


		// si erreur d'authentification (=FALSE, et non pas NULL), arrêter maintenant
		if ( self::$auth === FALSE )
			die("Erreur d'authentification");

        
        // output buffered
        ob_start();
        
        try{

            // affichage template
            Template::render(self::$USER, self::$CONTEXT, $cfg, $command);
            
        }
        finally
        {
            ob_end_flush();
        }
		
	}
}

?>