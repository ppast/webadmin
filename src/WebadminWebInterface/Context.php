<?php

// namespace
namespace Ppast\Webadmin\WebadminWebInterface;



// clauses use



// contexte interface
class Context 
{
	public $kind = NULL;
	public $value = NULL;
	public $userdata = NULL;
	
	
	public function __construct($kind, $value)
	{
		$this->kind = $kind;
		$this->value = $value;
		$this->userdata = [];
	}
	
	
	// pas de contexte
	static function emptyContext()
	{
		return new Context(NULL, NULL);
	}
	
	
	// ce contexte est-il initialisé ?
	public function noContext()
	{
		return is_null($this->kind);
	}
	
	
	// ce contexte est-il un profil (app)
	public function isProfile()
	{
		if ( $this->kind === 'app' )
			return $this->value;
		else
			return NULL;
	}
	
	
	// ce contexte est-il un domaine (dom)
	public function isDomain()
	{
		if ( $this->kind === 'dom' )
			return $this->value;
		else
			return NULL;
	}
	
	
	// ce contexte est-il un profil (webadmin)
	public function isWebadmin()
	{
		if ( $this->kind === 'webadmin' )
			return $this->value;
		else
			return NULL;
	}
}

?>