<?php

// namespace
namespace Ppast\Webadmin\WebadminWebInterface;



// clauses use
use \Ppast\Webadmin\Includes\RequestSecurityHelper;
use \Ppast\Webadmin\WebInterface\CoreController;




class Login 
{
	// affichage
	static function render($u)
	{
	    $token = RequestSecurityHelper::formToken();
	    ?>
		<form name="fLogin" class="uiForm consoleLogin" method="post" action="<?php echo CoreController::$routage; ?>">
			<p>Utilisateur : <input type="text" name="u" size="50" value="<?php echo $u; ?>"></p>
			<p>Mot de passe : <input type="password" name="fLogin_password" size="30"></p>
			<input type="hidden" name="fLogin_token" value="<?php echo $token; ?>">
			<p style="color:#CCC;">Horaire poste : 
				<script>
					var dt = new Date(); 
					document.write(nettools.jscore.leadingZero(dt.getHours()) + ':' + nettools.jscore.leadingZero(dt.getMinutes()) + ':' + nettools.jscore.leadingZero(dt.getSeconds())); 
				</script> 
				/ horaire serveur : <?php echo date("H:i:s"); ?>
				<script>
					var dtphp = <?php echo time(); ?>; 
					var dtjs = Math.floor(dt.valueOf()/1000); 
					if ( Math.abs(dtphp - dtjs) > 50 )
						document.write('<span style="color:red;"> - Attention, l\'horloge n\'est pas à l\'heure !</span>');
				</script>
			</p>
			<p style="margin-top:25px;"><input type="submit" value="Valider"></p>
		</form>
		<?php
	}
}

?>