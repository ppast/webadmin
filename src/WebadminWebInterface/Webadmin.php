<?php

// namespace
namespace Ppast\Webadmin\WebadminWebInterface;



// clauses use
use \Ppast\Webadmin\WebInterface\CoreController;





class Webadmin 
{
	// affichage
	static function render($u, Context $webadminctx, \Ppast\Webadmin\Config\Base $cfg = NULL)
	{
		$webadmin = $webadminctx->isWebadmin();


		// proposer actions indépendantes et liés à l'administration (superuser uniquement)
		if ( \Ppast\Webadmin\Config\Users::usersProvider()->isUserAuthorized($u, array(\Ppast\Webadmin\Config\Users::$SUPERUSER)) !== FALSE )
		{
			?>
			<li style="margin-top:30px;"><b><span class="profileDomainWebadmin <?php if ( $webadmin ) echo 'profileDomainWebadminActive'; ?>">Choisir actions webadmin</span> (<?php echo $webadmin; ?>) : </b>
			<a href="<?php echo CoreController::$routage; ?>?webadmin=utilisateurs&u=<?php echo $u ?>" data-csrf="1">Utilisateurs</a>
			<?php 
			
			// si gestion administration utilisateurs
			if ( $webadmin === 'utilisateurs' )
			{
				?>
				<li style="margin-top:15px;">Gestion :
					<a href="<?php echo CoreController::$routage; ?>?cmd=users_list&webadmin=<?php echo $webadmin; ?>&u=<?php echo $u; ?>" data-csrf="1">Liste des utilisateurs</a>
				</li>
				
				<?php
			}

		}
		
	}
}

?>