<?php

// namespace
namespace Ppast\Webadmin\WebadminWebInterface;



// clauses use
use \Ppast\Webadmin\Config\Core;
use \Ppast\Webadmin\Auth\AuthHandler;
use \Ppast\Webadmin\WebInterface\CoreController;



// page
class Template 
{
	static public function render($u, Context $ctx, \Ppast\Webadmin\Config\Base $cfg = NULL, \Ppast\Webadmin\Commands\Base $command = NULL)
	{
		$relativeroot = str_replace(Core::$ROOT, '', Core::$WEBADMIN_CORE_ROOT);
		$selfrelativeroot = str_replace(Core::$ROOT, '', Core::$WEBADMIN_ROOT);
		?>
		<!DOCTYPE html>
		<html>
		<head>
			<script src="/<? echo $relativeroot; ?>Includes/net-tools/js-core/js-core.min.js"></script>
			<script src="/<? echo $relativeroot; ?>Includes/net-tools/ui/ui.min.js"></script>
			<script src="/<? echo $relativeroot; ?>Includes/net-tools/ui/ui.desktop.min.js"></script>
			<link rel="stylesheet" href="/<? echo $relativeroot; ?>Includes/net-tools/ui/ui.min.css">
			<link rel="stylesheet" href="/<? echo $relativeroot; ?>Includes/net-tools/ui/ui.<?php echo (Core::$ROOT_USER_CFG->DARKTHEME ? 'dark':'yellow'); ?>-theme.min.css">
			<link rel="stylesheet" href="/<? echo $relativeroot; ?>Includes/net-tools/ui/ui.desktop.min.css">
			<link rel="stylesheet" href="/<? echo $relativeroot; ?>Includes/net-tools/ui/ui.desktop.<?php echo (Core::$ROOT_USER_CFG->DARKTHEME ? 'dark':'yellow'); ?>-theme.min.css">
			<link rel="stylesheet" href="/<? echo $relativeroot; ?>Includes/ui.min.css">
			<link rel="stylesheet" href="/<? echo $selfrelativeroot; ?>Includes/theme.min.css">
            <?php
            // gestion du thème sombre
            if ( Core::$ROOT_USER_CFG->DARKTHEME )
            {
                ?>
				<link rel="stylesheet" href="/<? echo $relativeroot; ?>Includes/ui.dark-theme.min.css">
                <link rel="stylesheet" href="/<? echo $selfrelativeroot; ?>Includes/dark-theme.min.css">
                <?php
            }
            ?>
			<title>Console d'administration</title>

			<script>
			function disableAs(div)
			{
				var div = document.getElementById(div);
				if ( div )
				{
					var as = div.getElementsByTagName('A');
					var asl = as.length;
					
					for ( var i = 0 ; i < asl ; i++ )
					{
						as[i].href = "javascript:void(0)";
						as[i].setAttribute('data-csrf', '0');
						as[i].onclick = function(){nettools.ui.desktop.dialog.notify('Action impossible pour l\'instant : profil sécurisé');};
					}
				}
			}
			
			
            // ajouter un traitement sur les balises A pour afficher un sablier le temps du POST
            function waitForRequestEnd()
            {
                document.body.style.cursor = 'wait';
				document.body.style.opacity = '0.25';
            }
				
			
			function __waitOnClickHandler(old, e)
			{
				waitForRequestEnd();
				if ( typeof old == 'function' )
					old.call(this, e);
			}
				
				
			function __waitOnClick(a)
			{
				a.onclick = __waitOnClickHandler.bind(a, a.onclick);
			}

				
			function _waitOnClick()
			{
				var hrefs = document.querySelectorAll("a[data-waitcursor='1']");
				var hrefsl = hrefs.length;
				
				for ( var h = 0 ; h < hrefsl ; h++ )
					__waitOnClick(hrefs[h]);			
			}
			
			
			// initialiser CSRF
			nettools.jscore.SecureRequestHelper.setCSRFCookieName('<? echo AuthHandler::$CSRFCookieName; ?>');
			nettools.jscore.SecureRequestHelper.setCSRFSubmittedValueName('token');
			nettools.jscore.SecureRequestHelper.autoReplaceLinksWithPOSTRequests();
			nettools.jscore.registerOnLoadCallback(_waitOnClick);

			</script>
		</head>
		
		<body>
		<?php
		
		// si commande exécutée, afficher statut et feedback
		if ( $command )
		{
			?><pre class="status"><?php echo ($command->getStatus() ? "OK":"ERREUR") . '  -  ' . $command->getMessage(); ?></pre>
			<?php
			if ( $command->hasBody() )
			{
				// si pas d'affichage automatique du feedback
				if ( !$command->getStatusMetadata('autofeedback') )
				{
					?>
					<a href="javascript:void(0)" onclick="var x = document.getElementById('feedback'); x.style.visibility='visible';x.style.display='block';">Afficher les d&eacute;tails</a>
					<pre style="visibility:hidden; display:none;" id="feedback"><?php $command->render(); ?></pre>
					<?php
				}
				else
				{
					?>
					<pre id="feedback"><?php $command->render(); ?></pre>
					<?php
				}
			}
		}
		
		?>
		
		
		<h1>Administration site : <img src="/<?php if ( !file_exists(Core::$ROOT . 'favicon.ico') ) echo 'console-'; ?>favicon.ico" width="24" height="24" align="absmiddle"></h1>
		<?
		
		// si pas encore authentifié
		if ( !CoreController::$auth )
			Login::render($u);
		else
		{
			?>
			<ul>
				<?php
				// affichage actions profil
				Profile::render($u, $ctx, $cfg);

				// affichage actions domaine
				Domain::render($u, $ctx, $cfg);

				// affichage actions webadmin
				Webadmin::render($u, $ctx, $cfg);
				?>
			</ul>
			<?php
		}
	}
}

?>