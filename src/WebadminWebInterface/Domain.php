<?php

// namespace
namespace Ppast\Webadmin\WebadminWebInterface;



// clauses use
use \Ppast\Webadmin\Config\WebAdminConfig;
use \Ppast\Webadmin\Includes\Tools;
use \Ppast\Webadmin\Auth\AuthHandler;
use \Ppast\Webadmin\WebInterface\CoreController;




class Domain 
{
	// affichage
	static function render($u, Context $domctx, \Ppast\Webadmin\Config\Base $cfg = NULL)
	{
		$dom = $domctx->isDomain();
		?>
		<li style="margin-top:30px;"><b><span class="profileDomainWebadmin <?php if ( $dom ) echo 'profileDomainWebadminActive'; ?>">Choisir domaine</span> (<?php echo $dom; ?>) : </b><?php 

		// afficher choix applications
		foreach ( WebAdminConfig::$DOMAINS as $k=>$a )
		{
			if ( \Ppast\Webadmin\Config\Users::usersProvider()->isUserAuthorized($u, $a->PROFILE_GRANT) !== FALSE )
			{
				echo "<a href=\"" . CoreController::$routage . "?dom=$k&u=$u\" data-csrf=\"1\">$k</a>, ";
			}
		}
		
		?></li>
		
		<?php
		// actions sur domaine choisi et superuser uniquement
		if ( $dom && \Ppast\Webadmin\Config\Users::usersProvider()->isUserAuthorized($u, array(\Ppast\Webadmin\Config\Users::$SUPERUSER)) !== FALSE )
		{
			?>

			<li style="margin-top:15px;">Sauvegarder le site : 
				<a href="<?php echo CoreController::$routage; ?>?cmd=dump_web&dom=<?php echo $dom; ?>&u=<?php echo $u; ?>&j=0" data-csrf="1">Créer la sauvegarde</a>
				- <a href="<?php echo CoreController::$routage; ?>?cmd=download_dump_web&dom=<?php echo $dom; ?>&u=<?php echo $u; ?>&j=0" data-csrf="1">Télécharger la sauvegarde</a>
				- <a href="<?php echo CoreController::$routage; ?>?cmd=delete_dump_web&dom=<?php echo $dom; ?>&u=<?php echo $u; ?>&j=0" data-csrf="1">Effacer la sauvegarde</a>
			</li>

			<li style="margin-top:15px;">Analyse de la sécurité : 
				<a href="<?php echo CoreController::$routage; ?>?cmd=websec_analyse_j&dom=<?php echo $dom; ?>&u=<?php echo $u; ?>&j=0" data-csrf="1">Ce jour</a>
				- <a href="<?php echo CoreController::$routage; ?>?cmd=websec_analyse_j&dom=<?php echo $dom; ?>&u=<?php echo $u; ?>&j=1" data-csrf="1">J-1</a>
				- <a href="<?php echo CoreController::$routage; ?>?cmd=websec_analyse_j&dom=<?php echo $dom; ?>&u=<?php echo $u; ?>&j=2" data-csrf="1">J-2</a>
				- <a href="<?php echo CoreController::$routage; ?>?cmd=websec_analyse_j&dom=<?php echo $dom; ?>&u=<?php echo $u; ?>&j=3" data-csrf="1">J-3</a>
			</li>
			<li>Analyse des user-agents :
				<a href="<?php echo CoreController::$routage; ?>?cmd=websec_ua_j&dom=<?php echo $dom; ?>&u=<?php echo $u; ?>&j=0" data-csrf="1">Ce jour</a>
				- <a href="<?php echo CoreController::$routage; ?>?cmd=websec_ua_j&dom=<?php echo $dom; ?>&u=<?php echo $u; ?>&j=1" data-csrf="1">J-1</a>
				- <a href="<?php echo CoreController::$routage; ?>?cmd=websec_ua_j&dom=<?php echo $dom; ?>&u=<?php echo $u; ?>&j=2" data-csrf="1">J-2</a>
				- <a href="<?php echo CoreController::$routage; ?>?cmd=websec_ua_j&dom=<?php echo $dom; ?>&u=<?php echo $u; ?>&j=3" data-csrf="1">J-3</a>
			</li>
			<li>Modification .htaccess :
				<a href="<?php echo CoreController::$routage; ?>?cmd=websec_hta_ip&dom=<?php echo $dom; ?>&u=<?php echo $u; ?>" data-csrf="1">IPs bloquées</a>
				- <a href="<?php echo CoreController::$routage; ?>?cmd=websec_hta_ua&dom=<?php echo $dom; ?>&u=<?php echo $u; ?>" data-csrf="1">user-agents bannis</a>
			</li>
			<li>Téléchargement du fichier log :
				<a href="<?php echo CoreController::$routage; ?>?cmd=websec_log_j&dom=<?php echo $dom; ?>&u=<?php echo $u; ?>&j=0" data-csrf="1">Ce jour</a>
				- <a href="<?php echo CoreController::$routage; ?>?cmd=websec_log_j&dom=<?php echo $dom; ?>&u=<?php echo $u; ?>&j=1" data-csrf="1">J-1</a>
				- <a href="<?php echo CoreController::$routage; ?>?cmd=websec_log_j&dom=<?php echo $dom; ?>&u=<?php echo $u; ?>&j=2" data-csrf="1">J-2</a>
				- <a href="<?php echo CoreController::$routage; ?>?cmd=websec_log_j&dom=<?php echo $dom; ?>&u=<?php echo $u; ?>&j=3" data-csrf="1">J-3</a> 
			</li>
			
			<?php

			// si composer activé dans config
			if ( $cfg->COMPOSER )
			{
				// librairie composer en cours
				$libc = array_key_exists('libc', $_REQUEST) ? $_REQUEST['libc'] : ''; 
				?>
				<script>
					
				// demander les paramètres de l'outil de mise à jour sourcetree
				function composer_updatetool(domain, user)
				{
					// poser la question du chemin des fichiers sources
					nettools.ui.desktop.dialog.promptPromise('Chemin du dossier de développement SourceTree', 'd:\\documents locaux\\sourcetree'). 
						then(
							function(source)
							{
								if ( !source )
									return;
								
								
								// poser la question du chemin 7zip
								nettools.ui.desktop.dialog.promptPromise('Chemin vers 7Zip', 'C:\\Program Files\\7-Zip\\7z.exe'). 
									then(
										function(z)
										{
											if ( !z )
												return false;

											// informer du lieu où poser le fichier téléchargé
											nettools.ui.desktop.dialog.notifyPromise("\nCopier le fichier téléchargé à l'emplacement '" + source + "'.<br><br>"). 
												then(
													function()
													{
														// voile gris et sablier
														waitForRequestEnd();
														
														
														// si choix ok
														nettools.jscore.SecureRequestHelper.post(
																// url
																'<? echo CoreController::$routage; ?>', 

																// post
																{
																	cmd : 'composer_updatetool',
																	dom : domain,
																	u : user,
																	sourcetree : source,
																	zip : z
																}
															);
													}
												);
										}
									);
							}
						);
				}
				
				
				// demander confirmation pour effacer un package en dev
				function composer_devdelete(domain, user, p)
				{
					nettools.ui.desktop.dialog.confirmPromise('Confirmer l\'effacement du package \'' + p + '\' en développement.'). 
						then(
							function()
							{
								// voile gris et sablier
								waitForRequestEnd();


								// si choix ok
								nettools.jscore.SecureRequestHelper.post(
										// url
										'<? echo CoreController::$routage; ?>', 

										// post
										{
											cmd : 'composer_devdelete',
											dom : domain,
											u : user,
											package : p
										}
									);
							}
						);
				}

					
				// demander les paramètres pour le fichier reg de l'outil de mise à jour sourcetree
				function composer_updatetoolreg(domain, user)
				{
					// poser la question du chemin des fichiers sources
					nettools.ui.desktop.dialog.promptPromise('Chemin du dossier de développement SourceTree', 'd:\\documents locaux\\sourcetree'). 
						then(
							function(source)
							{
								if ( !source )
									return false;

								// voile gris et sablier
								waitForRequestEnd();


								// si choix ok
								nettools.jscore.SecureRequestHelper.post(
										// url
										'<? echo CoreController::$routage; ?>', 

										// post
										{
											cmd : 'composer_updatetoolreg',
											dom : domain,
											u : user,
											sourcetree : source
										}
									);
							}
						);
				}
				</script>

				<li style="margin-top:15px;">Outils spécifiques : 
					<a href="javascript:void(0)" onclick="composer_updatetool('<?php echo $dom; ?>', '<?php echo $u; ?>'); return false;">Outil de mise à jour SourceTree</a>
					- <a href="javascript:void(0)" onclick="composer_updatetoolreg('<?php echo $dom; ?>', '<?php echo $u; ?>'); return false;">Fichier de registre à fusionner pour association '.zipdev'</a> 
				</li>

				<li style="margin-top:15px;">Dossier de développement Composer : 
					<?php
					
					// faire l'inventaire des dossiers vendor
					$dirs = glob(Tools::ensureTrailingSlash(Tools::ensureTrailingSlash(WebAdminConfig::$ROOT) . $cfg->COMPOSER_DEV_PATH) . '*', GLOB_ONLYDIR);
					$dev_packages = array();
					
					if ( is_array($dirs) )
					{
						// faire l'inventaire de chaque vendor/package 
						foreach ( $dirs as $vendor )
						{
							// extraire la partie vendor du chemin
							$vendor_name = substr(strrchr($vendor, '/'), 1);
							
							// inventaire du vendor
							$packages = glob($vendor . '/*', GLOB_ONLYDIR);
							if ( is_array($packages) )
							{
								foreach ( $packages as $package )
								{
									// extraire la partie name du chemin
									$package_name = substr(strrchr($package, '/'), 1);
									$dev_packages[] = "{$vendor_name}/{$package_name}";
								}
							}
						}
					}
					
					
					// pour tous les packages en dev
					foreach ( $dev_packages as $p )
					{
						echo $p;
						?> (
						<a href="javascript:void(0)" onclick="composer_devdelete('<?php echo $dom; ?>', '<?php echo $u; ?>', '<?php echo $p; ?>'); return false;">delete</a>
						- <a href="<?php echo CoreController::$routage; ?>?cmd=composer_devzip&dom=<?php echo $dom; ?>&u=<?php echo $u; ?>&package=<?php echo $p; ?>" data-csrf="1"  data-waitcursor="1">zip</a>
						) 
						
						-
						<?php
					}
					?>
				</li>
				
				<li>Dossier Composer : 
					<?php
					// pour toutes les bibliothèques composer
					foreach ( $cfg->COMPOSER_LIB as $lib )
					{
						?>
						<a href="<?php echo CoreController::$routage; ?>?cmd=composer_libc&dom=<?php echo $dom; ?>&u=<?php echo $u; ?>&libc=<?php echo $lib; ?>" data-csrf="1"><?php echo $lib; ?></a> -
						<?php
					}
					?>
				</li>
				<?php
				// si lib composer choisie
				if ( $libc ) 
				{
					// si dossier sécurisé, certaines actions ne sont pas dispo
					if ( Tools::testSecFolder(Tools::ensureTrailingSlash(Tools::ensureTrailingSlash(WebAdminConfig::$ROOT) . $libc)) )
					{
						?>
						</ul>
						<div class="webAdminSecOn" id="webAdminSecOnComposer">
						<ul>
						<?php
					}

					?>
					<script>
					<?php
					// lire composer.json
					if ( file_exists(Tools::ensureTrailingSlash(Tools::ensureTrailingSlash(WebAdminConfig::$ROOT) . $libc) . 'composer.json') )
						$composer_json = file_get_contents(Tools::ensureTrailingSlash(Tools::ensureTrailingSlash(WebAdminConfig::$ROOT) . $libc) . 'composer.json');
					else
						$composer_json = '';
					?>
					
					// lire json et décoder
					var json = `<?php echo $composer_json; ?>`;
					
					
					
					// enregistrer modif composer.json
					function composer_json_submit(form)
					{
						while ( form && (form.nodeName != 'FORM') )
						{
							form = form.parentNode; 
						}
						
						if ( !form ) 
							return false; 
						
						form.submit();
						return true;
					}
					
					
					// mise à jour globale : demander confirmation
					function composer_update(domain, user, lib, testmode)
					{
						nettools.ui.desktop.dialog.confirmPromise('Confirmer l\'exécution de la mise à jour de tous les packages ' + (testmode ? '(MODE TEST) ':'') + '?'). 
							then(
								function()
								{
									// voile gris et sablier
									waitForRequestEnd();


									nettools.jscore.SecureRequestHelper.post(
												// url
												'<? echo CoreController::$routage; ?>', 

												// post
												{
													cmd : 'composer_update',
													dom : domain,
													u : user,
													libc : lib,
													test : testmode ? 1 : 0
												}
											);
								}
							);
					}
					
					
					// ajouter un package
					function composer_require(domain, user, lib)
					{
						nettools.ui.desktop.dialog.promptPromise('Package à ajouter (vendor/name ou vendor/name:version)', ''). 
							then(
								function(p)
								{
									if ( !p.match(/^[a-zA-Z0-9_-]+\/[a-zA-Z0-9_-]+([ ]*:[ ]*.+)?$/) ) 
										nettools.ui.desktop.dialog.notify('Format incorrect'); 
									else 
									{
										// voile gris et sablier
										waitForRequestEnd();


										nettools.jscore.SecureRequestHelper.post(
												// url
												'<? echo CoreController::$routage; ?>', 

												// post
												{
													cmd : 'composer_require',
													dom : domain,
													u : user,
													libc : lib,
													package : p
												}
											);
									}
								}
							);
					}
					
					
					// envoyer commande perso
					function composer_customcommand(domain, user, lib)
					{
						nettools.ui.desktop.dialog.promptPromise('Commande Composer à envoyer ?', ''). 
							then(
								function(c)
								{
									if ( c )
									{
										// voile gris et sablier
										waitForRequestEnd();


										nettools.jscore.SecureRequestHelper.post(
												// url
												'<? echo CoreController::$routage; ?>', 

												// post
												{
													cmd : 'composer_customcommand',
													dom : domain,
													u : user,
													libc : lib,
													command : c
												}
											);
									}
								}
							);
					}
					
					
					// envoyer commande sur package avec arguments supplémentaires
					function composer_commandWithArgs(domain, user, lib, command)
					{
						// obtenir liste packages dans un format html
						var packages = _composer_packages();
						if ( !packages )
							return false;


						// poser la question
						nettools.ui.desktop.dialog.promptPromise('Indiquer les paramètres de la commande \'' + command + '\' sous la forme : \'num_package parametres [--options]\'' +
                                           '<br><br>----------------------------<br><br>' + packages.html + '<br>----------------------------<br><br>', '').
							then(
								function(choix)
								{
									var regs = choix.match(/^([0-9]+)(.*)$/);
									if ( !regs || (regs.length != 3) || (parseInt(regs[1]) > packages.count) )
									{
										nettools.ui.desktop.dialog.notify('Format commande incorrect');
										return false;
									}


									// voile gris et sablier
									waitForRequestEnd();


									// envoyer commande en remplaçant numéro
									nettools.jscore.SecureRequestHelper.post(
											// url
											'<? echo CoreController::$routage; ?>', 

											// post
											{
												cmd : 'composer_' + command,
												dom : domain,
												u : user,
												libc : lib,
												package : packages.items[regs[1]-1] + regs[2]
											}
										);
								}
							);
					}
					
					
					// envoyer commande perso sur 1 package
					function composer_customcommand1(domain, user, lib)
					{
						// obtenir liste packages dans un format html
						var packages = _composer_packages();
						if ( !packages )
							return false;


						// poser la question
						nettools.ui.desktop.dialog.promptPromise('Indiquer la commande sous la forme : \'commande num_package [--options]\'' + 
                                           '<br><br>----------------------------<br><br>' + packages.html + '<br>----------------------------<br><br>', ''). 
							then(
								function(choix)
								{
									var regs = choix.match(/^([a-z-]+) ([0-9]+)(.*)$/);
									if ( !regs || (regs.length != 4) || (parseInt(regs[2]) > packages.count) )
									{
										nettools.ui.desktop.dialog.notify('Format commande incorrect');
										return false;
									}


									// voile gris et sablier
									waitForRequestEnd();


									// envoyer commande en remplaçant numéro
									nettools.jscore.SecureRequestHelper.post(
											// url
											'<? echo CoreController::$routage; ?>', 

											// post
											{
												cmd : 'composer_customcommand',
												dom : domain,
												u : user,
												libc : lib,
												command : regs[1] + ' ' + packages.items[regs[2] - 1] + regs[3]
											}
										);
								}
							);
					}
					
					
					// ajouter token Github API
					function composer_githubapitoken(domain, user, lib)
					{
						nettools.ui.desktop.dialog.promptPromise('Jeton Github API à ajouter ?', ''). 
							then(
								function(tok)
								{
									if ( tok )
									{
										// voile gris et sablier
										waitForRequestEnd();


										nettools.jscore.SecureRequestHelper.post(
												// url
												'<? echo CoreController::$routage; ?>', 

												// post
												{
													cmd : 'composer_githubapitoken',
													dom : domain,
													u : user,
													libc : lib,
													apitoken : tok
												}
											);
									}
								}
							);
					}
					
					
					// formater liste des packages
					function _composer_packages()
					{
						js = JSON.parse(json);
						if ( !js )
						{
							nettools.ui.desktop.dialog.notify('composer.json illisible !');
							return false;
						}
						
						
						// si rien à faire
						if ( !js['require'] ) 
							return false;
						
						
						// inventaire clause REQUIRE (qui est un littéral objet, dont chaque propriété indique un package et sa valeur la version)
						//  require : { "vendor/package" : "version", "vendor2/package2" : "version2" }
						var lst = [];
						for ( var rq in js.require )
							lst.push(rq);
							
						// formater la liste
						var lstl = lst.length;
						var str = '';
						for ( var i = 0 ; i < lstl ; i++ )
							str = str + '[' + (i+1) + '] ' + lst[i] + "<br>";
							
						// renvoyer données formatées + infos
						return {
							count : lstl,
							items : lst,
							html : str
						};
					}
					
					
					// formater liste repositories
					function _composer_repositories()
					{
						js = JSON.parse(json);
						if ( !js )
						{
							nettools.ui.desktop.dialog.notify('composer.json illisible !');
							return false;
						}
						
						
						// si rien à faire
						if ( !js['repositories'] ) 
							return false;
						
						
						// inventaire clause REPOSITORIES (qui est tableau de littéraux objets)
						var repositoriesl = js.repositories.length;
						var str = '';
						var lst = [];
						for ( var i = 0 ; i < repositoriesl ; i++ )
						{
							str = str + '[' + (i+1) + '] ' + js.repositories[i].url + " [" + js.repositories[i].type.toUpperCase() + "]<br>";
							lst.push(i);
						}
							
						// renvoyer données formatées + infos
						return {
							count : repositoriesl,
							html : str,
							items : lst,
                            repositories : js.repositories
						};
					}
					
					
					// choix d'un package et fonction continuation
					function _composer_choix(packages, lib, cb)
					{
						// poser la question
						nettools.ui.desktop.dialog.promptPromise(lib + '<br><br>----------------------------<br><br>' + packages.html + '<br>----------------------------<br><br>', ''). 
							then(
								function(choix)
								{
									if ( choix && parseInt(choix) && (choix > 0) && (choix <= packages.count) )
										cb(packages.items[choix-1]);
								}
							);
					}
					
					
					// action sur 1 package
					function _composer_action(domain, user, lib, invite, commande)
					{
						// obtenir liste packages dans un format html
						var packages = _composer_packages();
						if ( !packages )
							return false;
						
						_composer_choix(packages, invite, function(p)
												{
													// voile gris et sablier
													waitForRequestEnd();


													nettools.jscore.SecureRequestHelper.post(
															// url
															'<? echo CoreController::$routage; ?>', 
															
															// post
															{
																cmd : commande,
																dom : domain,
																u : user,
																libc : lib,
																package : p
															}
														);
												}
											);
					}
					
					
					// supprimer un package
					function composer_remove(domain, user, libc)
					{
						_composer_action(domain, user, libc, 'Quel est le package à supprimer ?', 'composer_remove');
					}
					
					
					// mettre à jour un package
					function composer_update1(domain, user, libc)
					{
						_composer_action(domain, user, libc, 'Quel est le package à mettre à jour ?', 'composer_update1');
					}
					
					
					// infos sur un package
					function composer_show1(domain, user, libc)
					{
						_composer_action(domain, user, libc, 'Quel est le package à afficher ?', 'composer_show1');
					}
					
					
					// télécharger une archive
					function composer_archive(domain, user, libc)
					{
						_composer_action(domain, user, libc, 'Quel est le package à archiver?', 'composer_archive');
					}
					
					
					// expliquer pourquoi une mise à jour ne peut pas se faire
					function composer_prohibits(domain, user, libc)
					{
						composer_commandWithArgs(domain, user, libc, 'prohibits');
					}
					
					
					// dépendances de package
					function composer_showdependencies(domain, user, libc)
					{
						_composer_action(domain, user, libc, 'Quel est le package dont il faut afficher les dépendances ?', 'composer_showdependencies');
					}
					
					
					// quels sont les packages qui dépendent d'un package
					function composer_depends(domain, user, libc)
					{
						_composer_action(domain, user, libc, 'Quel est le package dont il faut afficher les packages qui en dépendent ?', 'composer_depends');
					}
					
					
					// quel est le package à clôner
					function composer_clonedev(domain, user, libc)
					{
						_composer_action(domain, user, libc, 'Quel est le package à clôner en développement ?', 'composer_clonedev');
					}
					
					
					// ajouter un repository
					function composer_repositories_add(domain, user, libc, lib, defaultpath, typerepo)
					{
						nettools.ui.desktop.dialog.promptPromise(lib, defaultpath). 
							then(
								function(path)
								{
									if ( !path )
										return false;


									// voile gris et sablier
									waitForRequestEnd();


									nettools.jscore.SecureRequestHelper.post(
											// url
											'<? echo CoreController::$routage; ?>', 

											// post
											{
												cmd : 'composer_repositoryadd',
												dom : domain,
												u : user,
												libc : libc,
												type : typerepo,
												url : path
											}
										);
								}
							);
					}
					
					
					// ajouter un repository type PATH
					function composer_repositories_add_path(domain, user, libc)
					{
						return composer_repositories_add(domain, user, libc, 
								'Quel est le chemin de développement du package à ajouter en repository PATH ?',
								'../<?php echo $cfg->COMPOSER_DEV_PATH; ?><vendor>/<name>',
								'path'
							);
					}
					
					
					// ajouter un repository type VCS
					function composer_repositories_add_vcs(domain, user, libc)
					{
						return composer_repositories_add(domain, user, libc, 
								'Quel est l\'url du package à ajouter en repository VCS ?',
								'https://<nom package>',
								'vcs'
							);
					}
					
					
					// supprimer un repository
					function composer_repositories_remove(domain, user, lib)
					{
						// obtenir liste repositories dans un format html
						var repositories = _composer_repositories();
						if ( !repositories )
							return false;
						
						_composer_choix(repositories, 'Quel est le repository à supprimer ?', function(idx)
												{
													// voile gris et sablier
													waitForRequestEnd();


													nettools.jscore.SecureRequestHelper.post(
															// url
															'<? echo CoreController::$routage; ?>', 
															
															// post
															{
																cmd : 'composer_repositoryremove',
																dom : domain,
																u : user,
																libc : lib,
																url : repositories.repositories[idx].url
															}
														);
												}
											);
					}
					
					
					// demander les paramètres pour webadmin en dev
					function composer_webadmindev(domain, user, lib)
					{
						// poser la question du chemin des fichiers sources
						nettools.ui.desktop.dialog.promptPromise('Chemin du nouveau dossier libc pour webadmin en DEV', '/libc-webadmindev'). 
							then(
								function(path)
								{
									if ( !path )
										return false;

									// voile gris et sablier
									waitForRequestEnd();


									// si choix ok
									nettools.jscore.SecureRequestHelper.post(
											// url
											'<? echo CoreController::$routage; ?>', 

											// post
											{
												cmd : 'composer_webadmindev',
												dom : domain,
												u : user,
												libcsource : lib,
												libcdest : path
											}
										);
								}
							);
					}
				
					</script>
					<li>Composer (<b><?php echo $libc; ?></b>) :
						<a href="<?php echo CoreController::$routage; ?>?cmd=composer_setup&dom=<?php echo $dom; ?>&u=<?php echo $u; ?>&libc=<?php echo $libc; ?>" data-csrf="1"  data-waitcursor="1">setup</a>
						- <a href="<?php echo CoreController::$routage; ?>?cmd=composer_selfupdate&dom=<?php echo $dom; ?>&u=<?php echo $u; ?>&libc=<?php echo $libc; ?>" data-csrf="1" data-waitcursor="1">self-update</a>
						- <a href="<?php echo CoreController::$routage; ?>?cmd=composer_install&dom=<?php echo $dom; ?>&u=<?php echo $u; ?>&libc=<?php echo $libc; ?>" data-csrf="1" data-waitcursor="1">install</a>
						- <a href="javascript:void(0)" onclick="composer_update('<?php echo $dom; ?>', '<?php echo $u; ?>', '<?php echo $libc; ?>', true); return false;">update (test)</a>
						- <a href="javascript:void(0)" onclick="composer_update('<?php echo $dom; ?>', '<?php echo $u; ?>', '<?php echo $libc; ?>', false); return false;">update</a>
						- <a href="<?php echo CoreController::$routage; ?>?cmd=composer_show&dom=<?php echo $dom; ?>&u=<?php echo $u; ?>&libc=<?php echo $libc; ?>" data-csrf="1" data-waitcursor="1">show</a>
						- <a href="<?php echo CoreController::$routage; ?>?cmd=composer_outdated&dom=<?php echo $dom; ?>&u=<?php echo $u; ?>&libc=<?php echo $libc; ?>" data-csrf="1" data-waitcursor="1">outdated</a>
						- <a href="<?php echo CoreController::$routage; ?>?cmd=composer_clearcache&dom=<?php echo $dom; ?>&u=<?php echo $u; ?>&libc=<?php echo $libc; ?>" data-csrf="1" data-waitcursor="1">clear-cache</a>
						- <a href="<?php echo CoreController::$routage; ?>?cmd=composer_diagnose&dom=<?php echo $dom; ?>&u=<?php echo $u; ?>&libc=<?php echo $libc; ?>" data-csrf="1" data-waitcursor="1">diagnose</a>
						- <a href="<?php echo CoreController::$routage; ?>?cmd=composer_archiveproject&dom=<?php echo $dom; ?>&u=<?php echo $u; ?>&libc=<?php echo $libc; ?>" data-csrf="1" data-waitcursor="1">archive</a>
						- <a href="javascript:void(0)" onclick="composer_githubapitoken('<?php echo $dom; ?>', '<?php echo $u; ?>', '<?php echo $libc; ?>'); return false;">set Github API token</a>
						<?php 
						if ( $libc === $cfg->COMPOSER_WEBADMIN )
						{
						?>
							- <a href="javascript:void(0)" onclick="composer_webadmindev('<?php echo $dom; ?>', '<?php echo $u; ?>', '<?php echo $libc; ?>'); return false;">webadmin en DEV</a>
						<?php
						}
						?>
						- <a href="javascript:void(0)" onclick="composer_customcommand('<?php echo $dom; ?>', '<?php echo $u; ?>', '<?php echo $libc; ?>'); return false;">custom command</a>
					</li>
					<li>
						Composer - packages  (<b><?php echo $libc; ?></b>) : <a href="javascript:void(0)" onclick="composer_require('<?php echo $dom; ?>', '<?php echo $u; ?>', '<?php echo $libc; ?>'); return false;">require</a>
						- <a href="javascript:void(0)" onclick="composer_remove('<?php echo $dom; ?>', '<?php echo $u; ?>', '<?php echo $libc; ?>'); return false;">remove</a>
						- <a href="javascript:void(0)" onclick="composer_update1('<?php echo $dom; ?>', '<?php echo $u; ?>', '<?php echo $libc; ?>'); return false;" >update</a>
						- <a href="javascript:void(0)" onclick="composer_show1('<?php echo $dom; ?>', '<?php echo $u; ?>', '<?php echo $libc; ?>'); return false;">show</a>
						- <a href="javascript:void(0)" onclick="composer_showdependencies('<?php echo $dom; ?>', '<?php echo $u; ?>', '<?php echo $libc; ?>'); return false;">show dependencies</a>
						- <a href="javascript:void(0)" onclick="composer_depends('<?php echo $dom; ?>', '<?php echo $u; ?>', '<?php echo $libc; ?>'); return false;">depends</a>
						- <a href="javascript:void(0)" onclick="composer_archive('<?php echo $dom; ?>', '<?php echo $u; ?>', '<?php echo $libc; ?>'); return false;">archive</a>
						- <a href="javascript:void(0)" onclick="composer_prohibits('<?php echo $dom; ?>', '<?php echo $u; ?>', '<?php echo $libc; ?>'); return false;">prohibits</a>
						- <a href="javascript:void(0)" onclick="composer_clonedev('<?php echo $dom; ?>', '<?php echo $u; ?>', '<?php echo $libc; ?>'); return false;" >clone to dev</a>
						- <a href="javascript:void(0)" onclick="composer_customcommand1('<?php echo $dom; ?>', '<?php echo $u; ?>', '<?php echo $libc; ?>'); return false;">custom command</a>
					</li>
					<li>
						Composer - repositories  (<b><?php echo $libc; ?></b>) : <a href="javascript:void(0)" onclick="composer_repositories_add_path('<?php echo $dom; ?>', '<?php echo $u; ?>', '<?php echo $libc; ?>'); return false;">ajouter type='path'</a>
						- <a href="javascript:void(0)" onclick="composer_repositories_add_vcs('<?php echo $dom; ?>', '<?php echo $u; ?>', '<?php echo $libc; ?>'); return false;">ajouter type='vcs'</a>
						- <a href="javascript:void(0)" onclick="composer_repositories_remove('<?php echo $dom; ?>', '<?php echo $u; ?>', '<?php echo $libc; ?>'); return false;">supprimer</a>
					</li>
					<form class="uiForm" name="fWebadmin_composer_json" method="post" action="<? echo CoreController::$routage; ?>">
						<?php echo AuthHandler::getCSRFHelper()->addCSRFHiddenInput(); ?>
						<input type="hidden" name="dom" value="<?php echo $dom; ?>">
						<input type="hidden" name="libc" value="<?php echo $libc; ?>">
						<input type="hidden" name="u" value="<?php echo $u; ?>">
						<input type="hidden" name="cmd" value="composer_json">
						<li>Composer.json (<b><?php echo $libc; ?></b>) : <a href="javascript:void(0)" onclick="return composer_json_submit(this.parentNode);">Enregistrer les modifications</a></li>
						<textarea name="composer_json" cols="80" rows="12" spellcheck="false"><?php echo $composer_json; ?></textarea>
					</form>
					<?php
					// si dossier sécurisé, certaines actions ne sont pas dispo
					if ( Tools::testSecFolder(Tools::ensureTrailingSlash(Tools::ensureTrailingSlash(WebAdminConfig::$ROOT) . $libc)) )
					{
						?>
						</ul>
						</div>
						<ul>
						<script>
						disableAs('webAdminSecOnComposer');
						</script>
						<?php
					}
				}
			}
		}
		
	}
}

?>