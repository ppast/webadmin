<?php


\Ppast\Webadmin\Config\Root::setup('root', array(

	// HTACCESS 
	'HTA__BEGIN_TAG_IP_DENY' => '# LISTE IP BLOQUEES',						// tag de début pour liste des IP bloquées dans le .htaccess
	'HTA__END_TAG_IP_DENY' => '# /LISTE IP BLOQUEES',						// tag de fin pour liste des IP bloquées dans le .htaccess
	'HTA__BEGIN_TAG_UA_DENY' => '# INTERDIRE ROBOTS SELON USER-AGENT',		// tag de début pour liste des user-agents bloqués dans le .htaccess
	'HTA__END_TAG_UA_DENY' => '# /USER-AGENTS',								// tag de fin pour liste des user-agents bloqués dans le .htaccess
	'HTA__UA' => '/SetEnvIfNoCase User-Agent "(.*)" bad_bot/',				// expression régulière pour extraire les user-agents
	'HTA__NEW_UA' => 'SetEnvIfNoCase User-Agent "%" bad_bot',				// masque pour ajout user-agent
	'HTA__NEW_IP' => ' deny from %',										// masque pour ajout nouvelle IP proscrite
	'HTA__TEST_HTACCESS' => '# DESACTIVATION INDEX ET GLOBALS PHP',			// tester contenu htaccess : début du fichier


	// OVH LOG
	'LOG__EXCLUDE_IP' => array(												// IPs à exclure (dont celle de ovh mutualisé, qui fait un HEAD pour vérif écriture HTA)
				'213.186.33.2', 	// ips ovh
				'213.186.33.3',
				'213.186.33.62',
				'213.186.33.19'
			),	
	'LOG__PREFIXES_OK' => array(											// url autorisés
		'^/apple-touch-icon(-[0-9]+x[0-9]+)?(-precomposed)?\.png$',
		'^/apple-app-site-association$',
		'^/.well-known/',
		'^/favicon\.ico$',
		'^//?robots\.txt$',
		'^/crossdomain\.xml$',
		'^/browserconfig\.xml$',
		'^/mobile/$',
		'^/m/$',
		'^/$'
	),


	// URL EXTERNES		
	'LOG__UA' => "http://www.google.fr/?#q=%&safe=off",									// URL pour interrogation google
	'LOG__NETWORK_TOOLS' => "https://myip.ms/info/whois/%",	// URL pour interrogation IP,


	// COMPOSER
	'COMPOSER_REQUIRE_DEV_OK' => 'Symlinking from',							// chaine à rechercher pour valider require dev ok
	'COMPOSER_REMOVE_OK' => 'Removing ',	 								// chaine à rechercher pour valider remove ok
	'COMPOSER_WEBADMIN_PACKAGES' => array('ppast/webadmin', 'ppast/webadmin_core'),		// packages webadmin pouvant être copiés en DEV pour modifs
	'COMPOSER_UPDATETOOL' => <<<'UPDTOOL'
@ECHO OFF
SET devdir=§devdir§
SET zip=§zip§

REM extraire nom de fichier, sans chemin
FOR /F %%I IN ("%1") DO SET fich=%%~nI 

REM extraire vendor-name
FOR /F "delims=! tokens=1,2" %%I IN ("%fich%") DO (
  SET vendor=%%I
  SET name=%%J
)


ECHO %vendor%
ECHO %name%
echo %devdir%

%zip% x -o"%devdir%\%vendor%\%name%\" -y "%1" *

mshta javascript:alert("Extraction effectuee !");close();
UPDTOOL
,
	
	'COMPOSER_UPDATETOOLREG' => mb_convert_encoding(<<<'UPDTOOLREG'
Windows Registry Editor Version 5.00

[HKEY_CLASSES_ROOT\zipdev]
@="Archive Composer à fusionner"

[HKEY_CLASSES_ROOT\zipdev\DefaultIcon]
@="%SystemRoot%\\system32\\zipfldr.dll"

[HKEY_CLASSES_ROOT\zipdev\shell]
@="open"

[HKEY_CLASSES_ROOT\zipdev\shell\open]
@="Fusionner avec SourceTree"

[HKEY_CLASSES_ROOT\zipdev\shell\open\command]
@="%devdir%\\sourcetree-update.cmd %1"

[HKEY_CLASSES_ROOT\.zipdev]
"PerceivedType"="compressed"
"Content Type"="application/x-zip-compressed"
@="zipdev"


UPDTOOLREG
, 'ISO-8859-1', 'UTF-8')
))


?>