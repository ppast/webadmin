# Webadmin - Notice d'utilisation #

Utilisation de Webadmin pour l'administration de sites


## Script appelant ##

Il suffit d'appeler la librairie avec le chemin vers le dossier de configuration


```
#!php

$root = $_SERVER['DOCUMENT_ROOT'];
include_once "${root}/libc-webadmin/vendor/autoload.php";

\Ppast\Webadmin\WebadminWebInterface\Controller::process(rtrim(dirname(__FILE__),'/') . '/data/');

```


## Dossier de configuration ##


```
#!

/
|
+--- data
|      |
|      +--- auth
|      |      |
|      |      + users.config
|      |
|      +--- config_profiles
|      |      |
|      |      + profile1.php
|      |      + profile2.php
|      |
|      +--- config_domains
|      |      |
|      |      + domain1.php
|      |
|      +--- config_plugins
|      |      |
|      |      + plugin1.php
|      |
|      +--- root_config
|      |      |
|      |      + user.php
|      |
|      +--- scripts_profiles
|      |      |
|      |      + script1.txt
|      |      + script2.txt
|      | 
|      +--- config_webadmins
|             |
|             + webadmin1.txt
|       
+--- plugins
       |
       + plugin1.php

```


## Première utilisation ##

Tant que le fichier des utilisateurs (users.config) n'a pas été initialisé, on rentre dans webadmin avec les identifiants admin/password.

Le fichier users.config doit comporter une première ligne avec les noms des propriétés, séparés par ';'. Les utilisateurs viennent ensuite, 
chaque propriété séparée des autres par ';'.
